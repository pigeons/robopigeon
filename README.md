# RoboPigeon

RoboPigeon is here to solve your CI problems. It's a set of tools that you can use to add complexity while simplfying your proccesses.

Some of the things it can do:

Send your team a slack notification when a job fails.

## Sponsors

<a href="https://granicus.com/careers/"><img alt="Granicus" src="https://granicus.com/wp-content/uploads/2018/07/logo.svg"  width="300"></a>

## Installation

This Gem is developed against ruby 2.5, but is tested in CI against the latest versions of `2.3` and `2.6`

Add this line to your application's Gemfile:

```ruby
gem 'robopigeon'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install robopigeon

## Usage

Best way to get started is by running `robopigeon init` and having it write the default robopigeon.rb file.

Then, you can reference the DSL below to make changes to your file.

## DSL Reference

Coming soon, stay tuned. In the mean time you can [look at our example file](https://gitlab.com/pigeons/robopigeon/blob/master/lib/robopigeon/resources/initial_robopigeon.rb).

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/pigeons/robopigeon.

### Contributors

[Alex Ives](https://alex.ives.mn)
[Erik Braegelmann](https://gitlab.com/ebraegelmann)
[Bill Bushey](https://gitlab.com/wbushey)

### History

RoboPigeon grew out of a tool at [Granicus](https://granicus.com) called `gitlab_tools`. As the company grew, the team that created it (the pigeon team) recognized the need for a more flexible tool, and thus robopigeon was born! We also realized that with more flexibility comes more usefulness to other people. Since we get so much from open source, we recognized this as an opportunity to give back.

### Deployment

Update the .version file to the desired version and merge to master. The gem will be cut and deployed to rubygems, and a tag will be created automatically.

## Future Development

### Features for 1.0

GitLab
- [x] Create, comment on, and merge merge requests
- [x] Get data from the gitlab deployments api as a helper
- [x] Add helpers to get a list of jira tickets since the provided deployment
- [x] Create Tag
- [x] Create Commits

Semver
- [x] Read a version from a file
- [x] Update version files
- [x] Updates package.json
- [x] Updates Android app build.gradle files
- [x] Updates pom.xml
- [x] Updates files by regex

Slack (To be deprecated)
- [x] Post notifications to channels or users
- [x] Post notifications with url button actions

Messaging
- [ ] Post notifications to channels or users
- [ ] Post notifications and wait for a response
- [ ] Add slack backend
- [ ] Add deprecation notice to slack calls

Jira
- [x] Create tickets in jira
- [x] Transition a ticket between states
- [x] Comment on tickets
- [x] Add fix version to ticket

Git
- [x] Add helpers to get information about changes since the last release

Jenkins
- [x] Kick off and monitor a jenkins job until it is completed

Extensions
- [x] Create basic example dsl extension gem template
- [ ] Extensions and other gems should be automatically installed by something like bundler based on the config

Docker
- [ ] Provide a docker image with robopigeon pre-installed

Documentation
- [ ] Generate or create human readable api documentation
- [ ] Document common tasks and setup
- [ ] Create example gitlab ci files
- [ ] Create example jenkins jobs
- [ ] Create example travis ci jobs
- [ ] Create example circle ci jobs

### Features for future version

Variables
- [ ] Add a variable configuration block that will inject them into all of the job contexts

PagerDuty
- [ ] Get current oncall user from pagerduty api

Risk Metrics Helpers
- [ ] Globally configure deploy risk metrics
- [ ] Output risk metrics as markdown via kramdown

Changelog
- [ ] Aggregates changes into log
- [ ] Helpers provide changes since last deployment
- [ ] Changelog yaml front matter informs risk of change

Deplyment Tracking
- [ ] New Relic
- [ ] Sentry
- [ ] Airbrake
- [ ] Stackdriver
- [ ] Bugsnag

GitLab
- [ ] Wait for a merge request to be ready to merge
- [ ] Kick off a pipeline
- [ ] Find a pipeline
- [ ] Wait on a pipeline until completion, failing if the pipeline fails
- [ ] Push subtree
- [ ] Create releases
- [ ] Create Issue
- [ ] Update issue tags
- [ ] Assign Issue
- [ ] Write file to wiki page
- [ ] Write to snippet

GitHub
- [ ] Create, comment on, and merge pull requests
- [ ] Create Tag
- [ ] Create Commits
- [ ] Create Release
- [ ] Push subtree
- [ ] Create Issue
- [ ] Update issue tags
- [ ] Assign Issue
- [ ] Write to Gist

Trello
- [ ] Create a card on a board
- [ ] Transition a card to a different bucket
- [ ] Comment on a card
- [ ] Create a checklist
- [ ] Check an item off on a checklist

Messages
- [ ] Add support for gitter backend
- [ ] Add support for email backend
- [ ] Add support for discord backend

Confluence
- [ ] Write Content to Confluence Page

MediaWiki
- [ ] Write Content to Wiki Page

Extensions
- [ ] Default jobs extension
- [ ] Jira ticket configuration extension
- [ ] Slack action template
- [ ] Gitlab issue configuration extension
- [ ] GitHub issue configuration extension

### Technical Debt

- [x] Document all the dsl methods with RoboPigeon::Documentarian
- [x] Re-organize code into modules by subject (eg: git, slack, gitlab, jira, ect)
- [ ] Generate human readable documentation from RoboPigeon::Documentarian
