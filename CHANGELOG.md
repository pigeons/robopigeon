## 0.5.0 (2019-05-16)

### Added

- Support for Jira fields that are arrays of options

## 0.4.0 (2019-04-29)

### Added

- Support for specifying Jira labels in jobs
- Support for Jira fields that are arrays of strings

### Fixed

- Does not attempt to send a Due Date field ot Jira if one does not exist

## 0.3.2 (2019-04-10)

### Added

- Allow configuration for how long to wait for designated jira state

## 0.3.1 (2019-04-8)

### Added

- Allow slack user methods to take an email and name
- Add helper for code change stats since deployment

### Fixed

- Show line numbers and file name in robopigeon.rb correctly
- Helpers are included even if defined after the main gem
- All dsls should extend base
- Fix jira ticket slack formatter issue
- Remove trailing slash from some lookups that Jira seems to dislike

## 0.3.0 (2019-04-8)

### Added

- Gitlab Helper to check if files had changed since deploy
- Gitlab Helper to get a link to a diff since that last deployment
- Init adds an rspec with an example to test your jobs

### Changed

- Tickets since deploy takes an optional argument to specify a regex match
- Creating a ticket writes out a file called 'last_created_jira_ticket' with the ticket id in it

## 0.2.0 (2019-03-29)

### Added (3 changes)

- Automatically generate extensions with `robopigoeon new [extension name]`
- DSL for making commits in gitlab projects via api
- DSL to trigger and watch jenkins jobs

## 0.1.0 (2019-03-12)

### Added

- DSL for creating and merging merge requests in gitlab
- DSL for creating jira tickets and waiting for them to transition
- DSL for sending slack messages to a person or channel
- Helper DSL for looking up slack user names
- Helper DSL for looking up last deployment information
- Helper DSL for reading and parsing markdown ERB files
