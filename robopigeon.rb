slack do
  enabled false
  api_key ENV['SLACK_API_KEY']
  name 'RoboPigeon'
end

gitlab do
  api_url 'https://gitlab.com/api/v4/'
  api_key ENV['GITLAB_API_KEY']
end

version do
  file '.version'
end

job 'notify_failure', 'Notify slack and gitlab that a job has failed' do
  slack do
    user ENV['GITLAB_USER_EMAIL']
    user git_committer_email
    attachment do
      fallback "robopigeon - #{ENV['CI_COMMIT_REF_NAME']} has a failure - <#{ENV['CI_PIPELINE_URL']}|Pipeline>"
      title "robopigeon - #{ENV['CI_COMMIT_REF_NAME']} has a failure"
      action 'button', 'Pipeline', ENV['CI_PIPELINE_URL'], 'primary'
      color 'danger'
    end
  end

  gitlab do
    merge_request_comment "The branch from this merge request has a failure - ENV['CI_PIPELINE_URL']", git_branch_merged_source
  end
end

job 'release_gem' do
  version do
    if ARGV[1] == 'major'
      increment_major
    elsif ARGV[1] == 'minor'
      increment_minor
    else
      increment_patch
    end
  end

  gitlab do
    tag(version, `git rev-parse HEAD`.strip, "Updating version to #{version}")
    puts "Tagged version #{version} via gitlab api"
  end

  slack do
    user ENV['GITLAB_USER_EMAIL']
    user git_committer_email
    user git_merger_email
    message "#{slack_user_for(ENV['GITLAB_USER_EMAIL'])} is deploying robopigeon version to rubygems.org"

    attachment do
      fallback "<#{ENV['CI_PIPELINE_URL']}|Pipeline>"
      action 'button', 'Pipeline', ENV['CI_PIPELINE_URL'], 'primary'
      color 'good'
    end
  end

  require 'open3'
  FileUtils.mkdir_p("#{ENV['HOME']}/.gem")
  File.write("#{ENV['HOME']}/.gem/credentials", { rubygems_api_key: ENV['RUBYGEMS_API_KEY'] }.to_yaml)
  FileUtils.chmod(0o600, "#{ENV['HOME']}/.gem/credentials")
  puts "Wrote gem credentials to #{ENV['HOME']}/.gem/credentials"

  push_output, _stderr, code = Open3.capture3('gem push robopigeon-*.gem')
  puts push_output
  exit 1 if code.exitstatus != 0
  puts 'Gem successfully pushed to rubygems.org'

  slack do
    user ENV['GITLAB_USER_EMAIL']
    user git_committer_email
    user git_merger_email
    message "#{slack_user_for(ENV['GITLAB_USER_EMAIL'])} successfully deployed robopigeon version to rubygems.org"

    attachment do
      fallback "<#{ENV['CI_PIPELINE_URL']}|Pipeline>"
      action 'button', 'Pipeline', ENV['CI_PIPELINE_URL'], 'primary'
      color 'good'
    end
  end
end
