lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'robopigeon/version'

Gem::Specification.new do |spec|
  spec.name          = 'robopigeon'
  spec.version       = RoboPigeon::VERSION
  spec.authors       = ['Alex Ives']
  spec.email         = ['alex@ives.mn']
  spec.licenses      = ['MIT']

  spec.summary       = 'A set of tools for gitlab and other ci piplines'
  spec.description   = 'Gitlab ci, jenkins, bamboo, circleci all leave something to be desired - a tigher integration with notifications and git actions. RoboPigeon is a new tool to help you craft pipelines that give you notifications and get you the functionality of a full time bot, without having to run one.'
  spec.homepage      = 'https://gitlab.com/pigeons/robopigeon'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = 'https://gitlab.com/pigeons/robopigeon'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) } + ['spec/initial_robopigeon_spec.rb']
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'gitlab', '~> 4.5'
  spec.add_dependency 'jenkins_api_client', '~> 1.5'
  spec.add_dependency 'markdown2confluence', '~> 0.0.4'
  spec.add_dependency 'nokogiri', '~> 1.10'
  spec.add_dependency 'picky', '~> 4.31'
  spec.add_dependency 'slack-ruby-client', '~> 0.14'
  spec.add_dependency 'slackdown', '~> 0.2'

  spec.add_development_dependency 'bundler', '>= 1.15', '< 3'
  spec.add_development_dependency 'bundler-audit', '~> 0.6'
  spec.add_development_dependency 'pry', '~> 0.12'
  spec.add_development_dependency 'rake', '~> 10.5'
  spec.add_development_dependency 'rspec', '~> 3.8'
  spec.add_development_dependency 'rubocop', '>= 0.74'
  spec.add_development_dependency 'simplecov', '~> 0.16'
  spec.add_development_dependency 'vcr', '~> 4.0'
  spec.add_development_dependency 'webmock', '~> 3.5'
end
