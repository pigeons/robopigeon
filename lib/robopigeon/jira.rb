RoboPigeon::Documentarian.add_block('jira', helpers: true, block: [], desc: 'a configuration block for jira')
RoboPigeon::Documentarian.add_block('jira', helpers: true, block: ['job'], desc: 'configure a jira ticket manipulation, sends on end, can have multiple')

require 'robopigeon/jira/client'
require 'robopigeon/jira/helper_dsl'
require 'robopigeon/jira/dsl'
require 'robopigeon/jira/ticket'
require 'robopigeon/jira/ticket_dsl'

module RoboPigeon::Jira
  class TicketNotFoundOrSet < StandardError; end
  class RequiredFieldNotSet < StandardError; end
  class FieldDoesNotConform < StandardError; end
  class WaitTimeout < StandardError; end
end

module RoboPigeon::Dsl
  module Helpers
    include RoboPigeon::Dsl::Helpers::Jira
  end
end

module RoboPigeon::Dsl
  class Root
    def jira(&block)
      RoboPigeon::Dsl::JiraRoot.run(&block)
    end
  end
end

module RoboPigeon::Dsl
  class Job
    def jira(&block)
      RoboPigeon::Dsl::Jira.run(&block)
    end
  end
end
