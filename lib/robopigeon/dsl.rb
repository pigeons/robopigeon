require 'robopigeon/dsl/helpers'
require 'robopigeon/dsl/job'
require 'robopigeon/dsl/base'
require 'robopigeon/dsl/initial_jobs'

module RoboPigeon::Dsl
  class Root
    include RoboPigeon::Dsl::InitialJobs
    attr_accessor :jobs, :file

    def initialize(file = RoboPigeon::DEFAULT_FILE)
      self.file = file
      self.jobs = {}
      base
      init_help
      init_new
      if File.exist?(file)
        contents = File.read(file)
        instance_eval(contents, file)
      else
        init_job
      end
    end

    def run(name)
      jobs[name][:action].call
    end

    def job?(name)
      !jobs[name].nil?
    end

    def usage
      max_size = jobs.keys.reject(&:nil?).max_by(&:length).size
      jobs_strings = jobs.keys.reject do |key|
        key.nil? || jobs[key][:hidden]
      end.map do |name|
        padding = ' ' * (max_size - name.length)
        "  '#{name}' #{padding}- #{jobs[name][:desc]}"
      end.join("\n")
      puts "Usage: robopigeon 'job name'"
      puts "Jobs are configured in #{file}"
      puts 'See https://gitlab.com/pigeons/robopigeon for configuration'
      puts "Configured Jobs:\n#{jobs_strings}"
    end
  end
end
