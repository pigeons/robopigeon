slack do
  # Slack is currently disabled, make sure you have a valid bot token, then remove
  # the following line
  enabled false
  # This is where you configure your global slack preferences, like bot name and api key
  # You should always keep the api key either encrypted with something like `encryptatron`
  # or in a ci environment variable (which is probably better)
  api_key ENV['SLACK_API_KEY']
  name 'RoboPigeon'
end

gitlab do
  # GitLab is currently disabled, make sure you have a valid api token with acces to
  # to your repo, then remove the following line
  enabled false
  # This is where you configure your global gitlab settings, like api url and api key
  # always keep your api key either in an environment variable or encrypted with something
  # something like `encryptatron` or rails secrets.
  api_url ENV['CI_API_V4_URL']
  api_key ENV['GITLAB_API_KEY'] # Always use an envrionment variable, don't check in secrets.
end

jira do
  # Jira is currently disabled, make sure you have a valid api token with acces to
  # to your repo, then remove the following line
  enabled false
  # This is where you configure your global jira settings, like api url and api token
  # always keep your api key either in an environment variable or encrypted with
  # something like `encryptatron` or rails secrets.
  api_url ENV['JIRA_API_URL']
  api_key ENV['JIRA_API_TOKEN'] # Always use an envrionment variable, don't check in secrets.
end

jenkins do
  # Jira is currently disabled, make sure you have a valid api token with acces to
  # to your repo, then remove the following line
  enabled false
  # This is where you configure your global jenkins settings, like api url and api token
  # always keep your api key either in an environment variable or encrypted with
  # something like `encryptatron` or rails secrets.
  api_url  'https://jenkins.example.com'
  api_user ENV['JENKINS_USER']
  api_key  ENV['JENKINS_TOKEN']
end

version do
  # In general, Files listed here are in order of preference. So the first will always
  # be treated as the source of truth if there is an error somewhere.

  # This is the most basic kind of version file, it just has the version semver in it
  # I recommend trying to use a .version file even if you have other ones so that you've
  # got one that won't accidentally get clobered by some other automated script.
  file '.version'
  pom_file 'pom.xml'

  # All of the types take a block, so you can manipulate the version for a specific file
  package_json_file 'package.json' do |version|
    # Say you always want your package.json file to be one minor version ahead of the
    # rest of your code for some crazy reason.
    "#{version.major}.#{version.minor + 1}.#{version.patch}" # Seriously, don't do that.
  end

  # Regex files allow you to update things by simply giving it a regex with a capture
  # group. The capture group will be replaced by the current version when you change it
  regex_file 'Version.java', /^    public static String VERSION = '(\d+\.\d+\.\d+)';$/

  # For android gradle files, you must provide a method to convert major, minor, patch
  # into a numeric versionCode for the play store It does not take a custom block for
  # manipulating the version because it needs the block for the versionCode
  android_gradle 'src/app.gradle' do |major, minor, patch|
    major * 1_000_000 + minor * 1_000 + patch
  end
end

# This job is an example of updating and tagging a new version of your project
# This one also shows how you can utilize cli arguments in your job, in this case
# we're using the first argument after the job name as what kind of version to change
job 'update_version', 'Used to update the version to a new one!' do
  version do
    if ARGV[1] == 'major'
      increment_major
    elsif ARGV[1] == 'minor'
      increment_minor
    else
      increment_patch
    end
  end

  gitlab do
    # Creates a commit
    commit do
      # Check in updated report file that helps split tests.
      file 'spec/knapsack_report.json'
      # Check in updates for all of the version files from the config
      version_files

      # You can configure a message for the commit too (this one uses the version helper)
      message "Updating version to #{version}"
      # And the author's name and email address (also can be auto-pulled from git)
      author 'Deployment Pigeon', 'pigeon@ives.dev'
      # What branch to commit to
      branch 'master'
    end

    # Now, tag that commit with the new version!
    create_tag('master', ENV['NEW_VERSION_NUMBER'], "Updating version to #{ENV['NEW_VERSION_NUMBER']}")
  end
end

# Here's an example job that runs something in jenkins.
job 'run_in_jenkins', 'Run a job in jenkins and make sure it passes' do
  # The jenkins block takes arguments to configure an existing job, you have
  # to at least specify a job name, and then everything else should be
  # configured through parameters.
  jenkins do
    # This tells the job to run a job in jenkins named 'robopigoen-test'
    name 'robopigeon-test'
    # This tells it to set a parameter called 'BRANCH' to the branch from gitlab ci
    param 'BRANCH', ENV['CI_COMMIT_REF_NAME']
  end
  # At this point the job will execute and then output the log to standard out so
  # so you can keep a consistent log
end

job 'deploy_complete', 'Notify slack and comment in gitlab that a deploy is complete' do
  # This configures a slack notification, at the end of the block it sends to
  # the users and channels in the body. Un-comment the channels and fill in the
  # appropriate ones to have them sent there.
  slack do
    # channel '#your-team-channel'
    # channel '#your-release-notifications-channel'
    user ENV['GITLAB_USER_EMAIL']
    # `git_committer_email` looks at the commit log and returns the email address
    # for the last person to make a non-merge commit
    user git_committer_email
    # `git_merger_email` looks at the commit log and returns the email address
    # for the last person to make a merge commit
    user git_merger_email
    # This configures the overall message to send, it comes first, before attachments.
    # If you want to send multiple messages, you can use attachments with pretext in them
    # It's not actually required to that you set a message in order to send attachments
    message "#{slack_user_for(ENV['GITLAB_USER_EMAIL'])} deployed #{ENV['CI_PROJECT_NAME']} #{ENV['CI_COMMIT_TAG']} to #{ENV['CI_ENVIRONMENT_NAME']}"
    attachment do
      # You have to set a fallback text for the actions
      fallback "<#{ENV['CI_PIPELINE_URL']}|Pipeline>"
      # Actions show up as little buttons under the message, this one will be
      # a green button that links to the pipeline.
      action 'button', 'Pipeline', ENV['CI_PIPELINE_URL'], 'primary'
      action 'button', 'Rollback', "#{ENV['CI_PROJECT_URL']}/environments", 'danger'
      color 'good'
    end
  end

  # This configures an action taken in gitlab. In this case, we're saying to use
  # the last branch merged into this one as the branch name, and then comment on
  # each merge request with that branch that someone has started deploying
  gitlab do
    merge_request_comment "#{ENV['GITLAB_USER_EMAIL']} deployed branch", git_branch_merged_source
  end

  # This configures an action taken in jira, In this case, we're looping over all
  # of the tickets since the last deployment, and then adding a comment to them
  # with what version it was released in.
  jira do
    # This helper gets all of the tickets referenced in the git log since the last
    # deployment to the referenced environment, that contain the provided key
    tickets_in_log_since_deployment_to('production', 'TIC').each do |ticket_id|
      # Then it comments on them with the project that was deployed and the version
      ticket ticket_id do
        # Then it comments on them with the project that was deployed and the version
        comment "#{ENV['CI_PROJECT_NAME']} had changes related to this deployed to production in version #{ENV['CI_COMMIT_TAG']}"
        # Runs a transition called 'Release'
        transition 'Release'
        # and sets the fix version to the tag of the deployment
        field 'Fix Version', ENV['CI_COMMIT_TAG']
      end
    end
  end
end

# This example job creates a ticket, assignes it to whoever runs the pipeline, and
# then sends them a slack message with the id of the ticket
job 'create ticket' do
  jira do
    ticket do
      # Set the project key
      project 'PIG'
      # Set the issue type
      issuetype 'Bug'
      # Set the summary of the ticket
      summary 'Detecetd a defect! Fix it!'
      # Assign the ticket to the user who is running the pipeline
      assign ENV['GITLAB_USER_EMAIL']
      # Set a description from a file template, that template can
      # use any of the other dsl helpers like the git or deployment
      # operations
      description jira_from_md('path/to/file.erb.md')
      # Create the ticket and store the id in a global variable
      create!
      # Print out the ticket number for logging/deubgging
      puts 'Created ticket:'
      print_id

      # This transitions the ticket using a transition called 'Start Work'
      transition 'Start Work'
    end
  end

  # Then it sends a notification in slack wit the link to the ticket
  slack do
    user ENV['GITLAB_USER_EMAIL']
    # That helper gives you a slack formatted link for the jira ticket
    message "Created #{jira_last_created_ticket_slack_link}"
  end
end

# Below are a number of pre-configured jobs that some people find useful!
job 'deploy_started', 'Notfiy slack and comment in gitlab that a deploy is starting' do
  slack do
    user ENV['GITLAB_USER_EMAIL']
    user git_committer_email
    user git_merger_email
    message "#{slack_user_for(ENV['GITLAB_USER_EMAIL'])} deployed #{ENV['CI_PROJECT_NAME']} #{ENV['CI_COMMIT_TAG']} to #{ENV['CI_ENVIRONMENT_NAME']}"
    attachment do
      fallback "<#{ENV['CI_PIPELINE_URL']}|Pipeline>"
      action 'button', 'Pipeline', ENV['CI_PIPELINE_URL'], 'primary'
      color 'good'
    end
  end

  gitlab do
    merge_request_comment "#{ENV['GITLAB_USER_EMAIL']} has started deyploying this branch", git_branch_merged_source
  end
end

job 'notify_failure', 'Notify slack and gitlab that a job has failed' do
  slack do
    # channel '#your-team-channel'
    user ENV['GITLAB_USER_EMAIL']
    user git_committer_email
    attachment do
      fallback "#{ENV['CI_PROJECT_NAME']} - #{ENV['CI_COMMIT_REF_NAME']} has a failure - <#{ENV['CI_PIPELINE_URL']}|Pipeline>"
      title "#{ENV['CI_PROJECT_NAME']} - #{ENV['CI_COMMIT_REF_NAME']} has a failure"
      action 'button', 'Pipeline', ENV['CI_PIPELINE_URL'], 'primary'
      color 'danger'
    end
  end

  gitlab do
    merge_request_comment "The branch from this merge request has a failure - ENV['CI_PIPELINE_URL']", git_branch_merged_source
  end
end
