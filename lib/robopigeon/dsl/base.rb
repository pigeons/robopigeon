module RoboPigeon::Dsl
  # this inherits from job, so that the job dsls are available in
  # extensions. That's very helpful if you're trying to write an
  # extension that interfaces with another extension or built in.
  class Base < RoboPigeon::Dsl::Job
    def respond_to_missing?(method, include_private)
      RoboPigeon::Dsl::Helpers.instance_methods(include_private).include?(method)
    end

    def method_missing(method, *args)
      self.class.class_exec do
        include RoboPigeon::Dsl::Helpers
      end
      if respond_to?(method)
        send(method, *args)
      else
        super
      end
    end
  end
end
