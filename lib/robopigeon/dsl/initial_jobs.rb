module RoboPigeon::Dsl::InitialJobs
  INITIAL_FILE = "#{File.dirname(__FILE__)}/../resources/initial_robopigeon.rb".freeze
  INITIAL_TEST = "#{File.dirname(__FILE__)}/../../../spec/initial_robopigeon_spec.rb".freeze
  TARGET_TEST = 'spec/robopigeon_spec.rb'.freeze

  # Used to pull in environment variables for things that are mostly set via dsl
  def base
    slack do
      api_key ENV['SLACK_API_KEY']
    end
    gitlab do
      api_url ENV['CI_API_V4_URL']
      api_key ENV['GITLAB_API_KEY']
    end
    job 'version', '-v', 'Show version number' do
      puts RoboPigeon::VERSION
    end
  end

  def init_job
    jobs['init'] = {
      desc: 'Initialize a new robopigeon project',
      action: proc do
        FileUtils.cp(INITIAL_FILE, RoboPigeon::DEFAULT_FILE)
        FileUtils.cp(INITIAL_TEST, TARGET_TEST)
      end
    }
  end

  def init_new
    jobs['new'] = {
      desc: 'Create a new extension for RoboPigeon',
      action: proc do
        RoboPigeon::Extensions::Cli.invoke(ARGV)
      end
    }
  end

  def init_help
    this_job = {
      desc: 'Show usage information',
      action: proc do
        usage
      end
    }
    jobs['help'] = this_job
    jobs['-h'] = this_job.merge(hidden: true)
  end
end
