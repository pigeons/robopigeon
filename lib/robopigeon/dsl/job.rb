RoboPigeon::Documentarian.add_block(
  'job',
  params: [
    { name: 'Name', type: 'String', desc: 'Name of the job', example: 'test_job' },
    { name: 'Description', type: 'String', desc: 'String describing what the job does', example: 'This job tests out some stuff while notifying slack' }
  ],
  helpers: true,
  block: [],
  desc: 'A grouping of commands to run'
)

module RoboPigeon::Dsl
  class Job
    include RoboPigeon::Dsl::Helpers

    def self.run(&block)
      instance = new
      instance.instance_eval(&block)
    end
  end
end

module RoboPigeon::Dsl
  class Root
    def self.run(&block)
      instance = new
      instance.instance_eval(&block)
    end

    def job(*args, &block)
      initial_name = args.shift
      this_job = {
        desc: args.pop,
        action: proc do
          job = RoboPigeon::Dsl::Job.new
          job.instance_eval(&block)
        end
      }
      jobs[initial_name] = this_job
      args.each do |arg|
        jobs[arg] = this_job.merge(hidden: true)
      end
    end
  end
end
