RoboPigeon::Documentarian.add_block('helpers', block: [], desc: 'A set of things available to all blocks')

module RoboPigeon::Dsl
  module Helpers
    RoboPigeon::Documentarian.add_command(
      'skip',
      block: ['helpers'],
      params: [
        { name: 'reason', type: 'String', desc: 'reason you want to skip this job', example: 'not configured' }
      ],
      desc: 'abort a job that is incomplete, raise an exception with the reason'
    )
    def skip(reason)
      raise SkippedJob, "Job is not complete: #{reason}"
    end
  end
end
