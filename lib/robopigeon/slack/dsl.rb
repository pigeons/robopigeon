module RoboPigeon::Dsl
  class SlackRoot < RoboPigeon::Dsl::Base
    RoboPigeon::Documentarian.add_command(
      'enabled',
      block: ['slack'],
      params: [
        { name: 'enabled', type: 'boolean', desc: 'should it run slack commands', example: 'false', default: 'true' }
      ],
      desc: 'Set whether or not to run slack commands, defaults to true'
    )
    def enabled(bool)
      RoboPigeon::Slack::Client.enabled = bool
    end

    RoboPigeon::Documentarian.add_command('api_key', block: ['slack'], params: [{ name: 'api_key', type: 'String', desc: 'the api key for your slackbot', example: "xoxb-don't-actually-store-this-here", default: "ENV['SLACK_API_KEY']" }], desc: 'set your slack api key globally')
    def api_key(key)
      RoboPigeon::Slack::Client.api_key = key
    end

    RoboPigeon::Documentarian.add_command('name', block: ['slack'], params: [{ name: 'name', type: 'String', desc: 'the name your slackbot displays', example: 'RoboPigeon 9000', default: 'RoboPigeon' }], desc: 'set your slack bot name globally')
    def name(name)
      RoboPigeon::Slack::Client.name = name
    end

    RoboPigeon::Documentarian.add_command('emoji', block: ['slack'], params: [{ name: 'emoji', type: 'String', desc: 'the emoji icon your slackbot displays', example: 'RoboPigeon 9000', default: 'RoboPigeon' }], desc: 'set your slack bot emoji icon globally')
    def emoji(emoji)
      RoboPigeon::Slack::Client.emoji = emoji
    end
  end

  class Slack < SlackRoot
    attr_accessor :message

    def self.run(&block)
      if RoboPigeon::Slack::Client.enabled
        slack = RoboPigeon::Dsl::Slack.new
        slack.instance_eval(&block)
        slack.message.send!
      else
        puts 'Slack is disabled, please remove `enabled false` from your global slack config'
      end
    end

    def initialize
      self.message = RoboPigeon::Slack::Message.new
    end

    RoboPigeon::Documentarian.add_command('user', block: %w[job slack], params: [{ name: 'search', type: 'String', desc: 'the name, email, or slack handle of a user to search for', example: 'robopigeon@ives.dev' }], desc: 'add a single user recipient to your message, can be used multiple times')
    def user(*user)
      message.users.push(RoboPigeon::Slack::Client.get_user(user).try(:id))
    end

    RoboPigeon::Documentarian.add_command('channel', block: %w[job slack], params: [{ name: 'channel', type: 'String', desc: 'the name of a channel to send the message to', example: '#team-pigeon' }], desc: 'add a single channel recipient to your message, can be used multiple times')
    def channel(channel)
      message.channels.push(channel)
    end

    RoboPigeon::Documentarian.add_command('text', block: %w[job slack], singular: true, params: [{ name: 'text', type: 'Text', desc: 'the text of the message you want to send', example: 'RoboPigeon failed a job! Oh no!' }], desc: 'Add a text message to the message you plan to send')
    def text(text)
      message.text = text
    end

    RoboPigeon::Documentarian.add_block('attachment', helpers: true, block: %w[job slack], desc: 'add an attachment to your slack message, can have multiple attachemnts')
    def attachment(&block)
      message.attachments.push(RoboPigeon::Dsl::SlackAttachment.run(&block))
    end
  end
end
