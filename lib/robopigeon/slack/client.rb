require 'slack-ruby-client'

module RoboPigeon::Slack
  class Client
    @name = 'RoboPigeon'
    @emoji = ':robot_face:'
    @enabled = true

    class << self
      attr_accessor :name, :emoji, :enabled

      def client
        raise 'client requires API key be set' unless @client

        @client
      end

      attr_writer :client

      def api_key
        raise 'api_key requires API key be set' unless client.token

        client.token
      end

      def api_key=(api_key)
        @client = Slack::Web::Client.new(token: api_key)
      end

      def get_user(search)
        lookup = search.shift
        begin
          users = client.users_search(user: lookup.downcase).try(:members) unless lookup.nil?
        rescue ::Faraday::Error => e
          puts "Giving up on slack user lookup because the slack client raised a #{e.class}:\n#{e.message}"
          users = nil
        end

        if users.nil? || users.empty? || users.length != 1
          return nil if search.empty?

          get_user(search)
        else
          users.try(:first)
        end
      end
    end
  end
end
