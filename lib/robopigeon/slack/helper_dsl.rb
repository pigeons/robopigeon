module RoboPigeon::Dsl
  module Helpers
    module Slack
      RoboPigeon::Documentarian.add_command('slack_user_for', block: ['helpers'], params: [{ name: 'search', type: 'String', desc: 'the name, email, or slack handle of a user to search for', example: 'robopigeon@ives.dev' }], desc: 'Searches for a given user and returns a formatted slack message mention')
      def slack_user_for(*search)
        uid = RoboPigeon::Slack::Client.get_user(search).try(:id)
        return '' if uid.nil?

        "<@#{uid}>"
      end

      RoboPigeon::Documentarian.add_command('slack_name_for', block: ['helpers'], params: [{ name: 'search', type: 'String', desc: 'the name, email, or slack handle of a user to search for', example: 'robopigeon@ives.dev' }], desc: 'Searches for a given user and returns their slack handle with an @')
      def slack_name_for(*search)
        uid = RoboPigeon::Slack::Client.get_user(search).try(:name)

        return '' if uid.nil?

        "@#{uid}"
      end

      RoboPigeon::Documentarian.add_command('slack_user_group', block: ['helpers'], params: [{ name: 'id', type: 'String', desc: 'the usergroup id to mention', example: 'robopigeon@ives.dev' }], desc: 'a message formatted mention of the given slack group id')
      def slack_user_group(id)
        "<!subteam^#{id}>"
      end
    end
  end
end
