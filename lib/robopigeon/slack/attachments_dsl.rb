module RoboPigeon::Dsl
  class SlackAttachment < RoboPigeon::Dsl::Base
    attr_accessor :attachment

    def initialize
      self.attachment = { actions: [] }
    end

    def self.run(&block)
      attachment = RoboPigeon::Dsl::SlackAttachment.new
      attachment.instance_eval(&block)
      attachment.attachment
    end

    private

    RoboPigeon::Documentarian.add_command('fallback', block: %w[job slack attachment], params: [{ name: 'text', type: 'String', desc: 'a string message', example: 'This message is displayed when the client does not support attachments' }], desc: 'Add a fallback message for clients that do not support actions')
    def fallback(text)
      attachment[:fallback] = text
    end

    RoboPigeon::Documentarian.add_command('title', block: %w[job slack attachment], params: [{ name: 'title', type: 'String', desc: 'the title of the attachment', example: 'Job Failed!' }], desc: 'Add a title to your attachment')
    def title(title)
      attachment[:title] = title
    end

    RoboPigeon::Documentarian.add_command('title_linke', block: %w[job slack attachment], params: [{ name: 'link', type: 'String', desc: 'a url', example: 'https://gitlab.com/pigeons/robopigeon/pipelines' }], desc: 'Add a link to the title so that users can click on it')
    def title_link(title_link)
      attachment[:title_link] = title_link
    end

    RoboPigeon::Documentarian.add_command('pretext', block: %w[job slack attachment], params: [{ name: 'text', type: 'String', desc: 'a string message', example: 'This message shows up before rest of the attachment' }], desc: 'Add a message that shows up just before your attachment')
    def pretext(text)
      attachment[:pretext] = text
    end

    RoboPigeon::Documentarian.add_command('color', block: %w[job slack attachment], params: [{ name: 'color', type: 'String', desc: 'a color, hex or something', example: 'danger' }], desc: 'Change the color of the bar next to your attachment')
    def color(color)
      attachment[:color] = color
    end

    RoboPigeon::Documentarian.add_command(
      'action',
      block: %w[job slack attachment],
      params: [
        { name: 'type', type: 'String', desc: 'Currently only supports `button`', example: 'button' },
        { name: 'text', type: 'String', desc: 'Field label', example: 'Pipeline' },
        { name: 'url', type: 'String', desc: 'Url to link to', example: 'https://gitlab.com/pigeons/robopigeon/pipelines' },
        { name: 'style', type: 'String', desc: 'button style, can be danger or primary', example: 'danger', defaut: 'nil' }
      ],
      desc: 'Add a Url linked button to your bot'
    )
    def action(type, text, url, style=nil)
      attachment[:actions].push(
        type: type,
        text: text,
        url: url,
        style: style
      )
    end
  end
end
