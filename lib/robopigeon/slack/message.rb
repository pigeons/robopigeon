module RoboPigeon::Slack
  class Message
    attr_accessor :users, :channels, :text, :attachments
    def initialize
      self.attachments = []
      self.users = []
      self.channels = []
    end

    def client
      RoboPigeon::Slack::Client
    end

    def send_message(recipient)
      raise 'No text or attachments set' if (text.nil? || text.empty?) && attachments.empty?

      client.client.chat_postMessage(
        channel: recipient,
        text: text,
        icon_emoji: client.emoji,
        username: client.name,
        attachments: attachments
      )
    end

    def send!
      users.reject(&:nil?).each do |user|
        send_message(user)
      end
      channels.reject(&:nil?).each do |channel|
        send_message(channel)
      end
    end
  end
end
