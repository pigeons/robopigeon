RoboPigeon::Documentarian.add_block(
  'version',
  helpers: true,
  block: %w[job],
  desc: 'Create a commit on a branch'
)
RoboPigeon::Documentarian.add_block(
  'version',
  helpers: true,
  block: [],
  desc: 'Create a commit on a branch'
)

require 'robopigeon/versioner/version'
require 'robopigeon/versioner/semver'
require 'robopigeon/versioner/version_file'
require 'robopigeon/versioner/pom'
require 'robopigeon/versioner/regex'
require 'robopigeon/versioner/android_gradle'
require 'robopigeon/versioner/package_json'
require 'robopigeon/versioner/helper_dsl'
require 'robopigeon/versioner/dsl'

module RoboPigeon::Dsl
  module Helpers
    include RoboPigeon::Dsl::Helpers::Versioner
  end
end

module RoboPigeon::Dsl
  class Root
    def version(&block)
      RoboPigeon::Dsl::VersionerRoot.run(&block)
    end
  end
end

module RoboPigeon::Dsl
  class Job
    def version(&block)
      RoboPigeon::Dsl::Versioner.run(&block)
    end
  end
end
