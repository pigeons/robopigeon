module RoboPigeon::Dsl
  module Helpers
    module Git
      RoboPigeon::Documentarian.add_command('git_committer_name', block: ['helpers'], desc: 'name of the last person to make a non-merge commit')
      def git_committer_name
        `git log -1 --pretty=format:'%an' --no-merges`
      end

      RoboPigeon::Documentarian.add_command('git_committer_email', block: ['helpers'], desc: 'email of the last person to make a non-merge commit')
      def git_committer_email
        `git log -1 --pretty=format:'%ae' --no-merges`
      end

      RoboPigeon::Documentarian.add_command('git_merger_name', block: ['helpers'], desc: 'name of the last person to make a merge commit')
      def git_merger_name
        `git log -1 --pretty=format:'%an' --merges`
      end

      RoboPigeon::Documentarian.add_command('git_merger_email', block: ['helpers'], desc: 'email of the last person to make a merge commit')
      def git_merger_email
        `git log -1 --pretty=format:'%ae' --merges`
      end

      RoboPigeon::Documentarian.add_command('git_branch_merged_source', block: ['helpers'], desc: 'source branch in the last merge commit')
      def git_branch_merged_source
        get_merge_data[:source]
      end

      RoboPigeon::Documentarian.add_command('git_branch_merged_target', block: ['helpers'], desc: 'target branch in the last merge commit')
      def git_branch_merged_target
        get_merge_data[:target]
      end

      RoboPigeon::Documentarian.add_command(
        'changed_since?',
        block: ['helpers'],
        params: [
          { name: 'ref', type: 'String', example: 'd72f9e3808a9e6d59ac88e3d56b81f073c5cca37', desc: 'Git ref to compare to' },
          { name: 'files', type: 'String', example: 'build.gradle', desc: 'file to check if changed' }
        ],
        desc: 'target branch in the last merge commit'
      )
      def changed_since?(ref, *files)
        `git diff #{ref} -- #{files.join(' ')}`
      end

      private

      def merge_commit_subject
        `git log -1 --merges --pretty=format:'%s'`
      end

      def get_merge_data
        commit_subject = merge_commit_subject || ''
        if data = commit_subject.match(/Merge branch '(.*)' into '(.*)'/)
          return {
            source: data[1],
            target: data[2]
          }
        elsif data = commit_subject.match(/Merge branch '(.*)' of (.*)/)
          return {
            source: "#{data[2]}/#{data[1]}",
            target: data[1]
          }
        end
        {
          source: 'unknown',
          target: 'unknown'
        }
      end
    end
  end
end
