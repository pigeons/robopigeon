RoboPigeon::Documentarian.add_block('jenkins', helpers: true, block: [], desc: 'a configuration block for jenkins')
RoboPigeon::Documentarian.add_block('jenkins', helpers: true, block: ['job'], desc: 'configure a jenkins job to create, run, and watch')

require 'robopigeon/jenkins/client'
require 'robopigeon/jenkins/job'
require 'robopigeon/jenkins/dsl'

module RoboPigeon::Dsl
  class Root
    def jenkins(&block)
      RoboPigeon::Dsl::JenkinsRoot.run(&block)
    end
  end
end

module RoboPigeon::Dsl
  class Job
    def jenkins(&block)
      RoboPigeon::Dsl::Jenkins.run(&block)
    end
  end
end
