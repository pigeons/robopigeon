module RoboPigeon::Versioner
  class VersionFile
    attr_accessor :object_type, :file, :version_lambda
    def initialize(file)
      raise "You must create version file #{file} to continue" unless file && File.exist?(file)

      self.file = file
      self.version_lambda = if block_given?
                              -> { yield RoboPigeon::Versioner::Version.current }
                            else
                              -> { RoboPigeon::Versioner::Version.current.to_s }
                            end
    end

    def read
      read_version = if block_given?
                       yield
                     else
                       File.read(file).chomp
                     end
      raise "Version file #{file} must be in the format /[0-9]+\\.[0-9]+\\.[0-9]+/ but was #{read_version}" unless read_version.match(/^\d+\.\d+\.\d+$/)

      RoboPigeon::Versioner::Semver.new(read_version)
    end

    def updated(_=false)
      version_lambda.call
    end

    def write
      File.write(file, updated)
    end

    def self.from_type(file, type: :plain, snapshot_flag: RoboPigeon::Versioner::Version::DEFAULT_SNAPSHOT_FLAG, regex: RoboPigeon::Versioner::Version::DEFAULT_FILE_REGEX)
      case type
      when :plain
        if block_given?
          RoboPigeon::Versioner::VersionFile.new(file, &Proc.new)
        else
          RoboPigeon::Versioner::VersionFile.new(file)
        end
      when :pom
        if block_given?
          RoboPigeon::Versioner::Pom.new(file, snapshot_flag, &Proc.new)
        else
          RoboPigeon::Versioner::Pom.new(file, snapshot_flag)
        end
      when :package_json
        if block_given?
          RoboPigeon::Versioner::PackageJson.new(file, &Proc.new)
        else
          RoboPigeon::Versioner::PackageJson.new(file)
        end
      when :regex
        if block_given?
          RoboPigeon::Versioner::Regex.new(file, regex, &Proc.new)
        else
          RoboPigeon::Versioner::Regex.new(file, regex)
        end
      when :android_gradle
        RoboPigeon::Versioner::AndroidGradle.new(file, &Proc.new)
      end
    end
  end
end
