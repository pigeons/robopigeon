module RoboPigeon::Versioner
  class Pom < RoboPigeon::Versioner::VersionFile
    attr_accessor :snapshot_flag
    def initialize(file, snapshot_flag=Version::DEFAULT_SNAPSHOT_FLAG)
      super(file)
      self.snapshot_flag = snapshot_flag
    end

    def read
      super do
        Nokogiri::Slop(File.read(file)).project.version.content.gsub(snapshot_flag, '')
      end
    end

    def updated(include_snapshot=false)
      version = super()

      xml = Nokogiri::Slop File.read(file)
      xml.project.version.content = version
      xml.project.version.content = "#{version}#{snapshot_flag}" if include_snapshot

      xml.to_xml
    end

    def write(include_snapshot=false)
      File.write(file, updated(include_snapshot))
    end
  end
end
