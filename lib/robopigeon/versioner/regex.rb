module RoboPigeon::Versioner
  class Regex < RoboPigeon::Versioner::VersionFile
    attr_accessor :regex
    def initialize(file, regex=/^(\d+\.\d+\.\d+)$/)
      super(file)
      self.regex = regex
    end

    def read
      super do
        File.read(file).match(regex)[1]
      end
    end

    def updated(_=false)
      contents = File.read(file)
      match = contents.match(regex)
      newline = match[0].gsub(match[1], super.to_s)
      contents.gsub(match[0], newline)
    end
  end
end
