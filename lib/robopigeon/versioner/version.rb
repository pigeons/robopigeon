module RoboPigeon::Versioner
  class Version
    DEFAULT_SNAPSHOT_FLAG = '-SNAPSHOT'.freeze
    DEFAULT_FILE_REGEX = /(\d+\.\d+\.\d+)/.freeze
    @files = []
    @current = nil

    class << self
      attr_accessor :files, :current

      # rubocop:disable Lint/DuplicateMethods
      def current
        return @current = nil if files.empty?

        @current ||= files.first.read
      end
      # rubocop:enable Lint/DuplicateMethods

      def add_all_to_commit(commit, snapshot=false)
        files.each do |file|
          commit.add_file(file.file, file.updated(snapshot))
        end
      end
    end
  end
end
