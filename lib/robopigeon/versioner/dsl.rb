require 'gitlab'
module RoboPigeon::Dsl
  class VersionerRoot < RoboPigeon::Dsl::Base
    RoboPigeon::Documentarian.add_command(
      'file',
      block: ['version'],
      params: [
        {
          name: 'filename',
          type: 'string',
          desc: 'filename where version is stored in plaintext x.x.x',
          example: '.version'
        },
        {
          name: 'options',
          type: 'hash',
          desc: 'type: one of [:plain, :pom, :package_json, :regex]; snapshot_flag: default -SNAPSHOT; regex: required for regex files',
          example: 'type: :pom',
          default: "type: :plain, snapshot_flag: #{RoboPigeon::Versioner::Version::DEFAULT_SNAPSHOT_FLAG}, regex: #{RoboPigeon::Versioner::Version::DEFAULT_FILE_REGEX}"
        }
      ],
      desc: 'Add a plain or other file to the list of version files in your repo'
    )
    def file(filename, type: :plain, snapshot_flag: RoboPigeon::Versioner::Version::DEFAULT_SNAPSHOT_FLAG, regex: RoboPigeon::Versioner::Version::DEFAULT_FILE_REGEX)
      RoboPigeon::Versioner::Version.files << RoboPigeon::Versioner::VersionFile.from_type(filename, type: type, snapshot_flag: snapshot_flag, regex: regex)
    end

    RoboPigeon::Documentarian.add_command(
      'pom_file',
      block: ['version'],
      params: [
        {
          name: 'filename',
          type: 'string',
          desc: 'filename of your pom file',
          example: 'pom.xml'
        },
        {
          name: 'snapshot_flag',
          type: 'string',
          desc: 'the flag used to signify a snapshot',
          example: RoboPigeon::Versioner::Version::DEFAULT_SNAPSHOT_FLAG,
          default: RoboPigeon::Versioner::Version::DEFAULT_SNAPSHOT_FLAG
        }
      ],
      desc: 'Add a pom file to store and update version information from'
    )
    def pom_file(filename, snapshot_flag=RoboPigeon::Versioner::Version::DEFAULT_SNAPSHOT_FLAG)
      RoboPigeon::Versioner::Version.files << if block_given?
                                                RoboPigeon::Versioner::Pom.new(filename, snapshot_flag, &Proc.new)
                                              else
                                                RoboPigeon::Versioner::Pom.new(filename, snapshot_flag)
                                              end
    end

    RoboPigeon::Documentarian.add_command(
      'package_json_file',
      block: ['version'],
      params: [
        {
          name: 'filename',
          type: 'string',
          desc: 'filename of your package.json',
          example: 'package.json'
        }
      ],
      desc: 'Add a package.json file to store and update version information from'
    )
    def package_json_file(filename)
      RoboPigeon::Versioner::Version.files << if block_given?
                                                RoboPigeon::Versioner::PackageJson.new(filename, &Proc.new)
                                              else
                                                RoboPigeon::Versioner::PackageJson.new(filename)
                                              end
    end

    RoboPigeon::Documentarian.add_command(
      'regex_file',
      block: ['version'],
      params: [
        {
          name: 'filename',
          type: 'string',
          desc: 'filename that you want to regex out a version',
          example: 'Version.java'
        },
        {
          name: 'regex',
          type: 'regular expression',
          desc: 'regex with a single capture group that matches the version semver string',
          example: RoboPigeon::Versioner::Version::DEFAULT_FILE_REGEX,
          default: RoboPigeon::Versioner::Version::DEFAULT_FILE_REGEX
        }
      ],
      desc: 'Add a strange file to store and update version information from'
    )
    def regex_file(filename, regex=RoboPigeon::Versioner::Version::DEFAULT_FILE_REGEX)
      RoboPigeon::Versioner::Version.files << if block_given?
                                                RoboPigeon::Versioner::Regex.new(filename, regex, &Proc.new)
                                              else
                                                RoboPigeon::Versioner::Regex.new(filename, regex)
                                              end
    end

    RoboPigeon::Documentarian.add_command(
      'android_gradle_file',
      block: ['version'],
      params: [
        {
          name: 'filename',
          type: 'string',
          desc: 'filename of your build.gradle',
          example: 'app/build.gradle'
        }
      ],
      desc: 'Add a android app build.gradle file to store and update version information from'
    )
    def android_gradle_file(filename)
      raise 'The android versionCode requires a block' unless block_given?

      RoboPigeon::Versioner::Version.files << RoboPigeon::Versioner::AndroidGradle.new(filename, &Proc.new)
    end
  end

  class Versioner < VersionerRoot
    RoboPigeon::Documentarian.add_command(
      'increment_major',
      block: %w[job version],
      desc: 'Increment the major version in the cache so that it can be written'
    )
    def increment_major
      RoboPigeon::Versioner::Version.current.increment_major
    end

    RoboPigeon::Documentarian.add_command(
      'increment_minor',
      block: %w[job version],
      desc: 'Increment the minor version in the cache so that it can be written'
    )
    def increment_minor
      RoboPigeon::Versioner::Version.current.increment_minor
    end

    RoboPigeon::Documentarian.add_command(
      'increment_patch',
      block: %w[job version],
      desc: 'Increment the patch version in the cache so that it can be written'
    )
    def increment_patch
      RoboPigeon::Versioner::Version.current.increment_patch
    end
  end
end
