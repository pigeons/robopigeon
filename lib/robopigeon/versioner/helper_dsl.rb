module RoboPigeon::Dsl
  module Helpers
    module Versioner
      RoboPigeon::Documentarian.add_command(
        'version',
        block: ['helpers'],
        desc: 'get the current version listed in git'
      )
      def version
        RoboPigeon::Versioner::Version.current.to_s
      end
    end
  end
end
