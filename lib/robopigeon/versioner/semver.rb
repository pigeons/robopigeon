module RoboPigeon::Versioner
  class Semver
    GIT_DESCRIBE_TAG_COMMAND = 'git describe --abbrev=0 --tags --match "[0-9]*.[0-9]*.[0-9]*"'.freeze
    attr_accessor :version

    def initialize(version)
      self.version = if version.is_a? String
                       version.split('.').map(&:to_i)
                     else
                       version
                     end
    end

    def major
      version[0]
    end

    def increment_major
      version[0] += 1
      version[1] = 0
      version[2] = 0
      to_s
    end

    def minor
      version[1]
    end

    def increment_minor
      version[1] += 1
      version[2] = 0
      to_s
    end

    def patch
      version[2]
    end

    def increment_patch
      version[2] += 1
      to_s
    end

    def to_s
      version.join('.')
    end

    def ==(other)
      if other.is_a? Array
        other.length == 3 && other == version
      elsif other.is_a? Hash
        other.length == 3 && other[:major] == major && other[:minor] == minor && other[:patch] == patch
      elsif other.is_a? RoboPigeon::Versioner::Semver
        other.version == version
      elsif other.is_a? String
        other == to_s
      else
        super
      end
    end
  end
end
