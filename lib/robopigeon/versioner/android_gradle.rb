module RoboPigeon::Versioner
  class AndroidGradle < RoboPigeon::Versioner::VersionFile
    NAME_REGEX = /^ +versionName +\"(\d+\.\d+\.\d+)\"$/.freeze
    CODE_REGEX = /^ +versionCode +(\d+)$/.freeze
    def initialize(file)
      raise "You must create version file #{file} to continue" unless file && File.exist?(file)
      raise 'You must provide a block to convert the semver into an integer version for the play store' unless block_given?

      self.file = file
      self.version_lambda = -> { yield(RoboPigeon::Versioner::Version.current.major, RoboPigeon::Versioner::Version.current.minor, RoboPigeon::Versioner::Version.current.patch) }
    end

    def read
      super do
        File.read(file).match(NAME_REGEX)[1]
      end
    end

    def updated(_=false)
      contents = File.read(file)
      updated_code(updated_name(contents))
    end

    private

    def updated_code(contents)
      match = contents.match(CODE_REGEX)
      newline = match[0].gsub(match[1], version_lambda.call.to_s)
      contents.gsub(match[0], newline)
    end

    def updated_name(contents)
      match = contents.match(NAME_REGEX)
      newline = match[0].gsub(match[1], RoboPigeon::Versioner::Version.current.to_s)
      contents.gsub(match[0], newline)
    end
  end
end
