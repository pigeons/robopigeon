require 'json'

module RoboPigeon::Versioner
  class PackageJson < RoboPigeon::Versioner::VersionFile
    def read
      super do
        JSON.parse(File.read(file).chomp)['version']
      end
    end

    def updated(_=false)
      json = JSON.parse(File.read(file).chomp)
      json['version'] = super

      json.to_json
    end
  end
end
