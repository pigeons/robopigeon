RoboPigeon::Documentarian.add_block('slack', helpers: true, block: [], desc: 'a configuration block for slack')
RoboPigeon::Documentarian.add_block('slack', helpers: true, block: ['job'], desc: 'configure a slack notification, sends on end, can have multiple')

require 'robopigeon/slack/client'
require 'robopigeon/slack/message'
require 'robopigeon/slack/dsl'
require 'robopigeon/slack/helper_dsl'
require 'robopigeon/slack/attachments_dsl'

module RoboPigeon::Dsl
  module Helpers
    include RoboPigeon::Dsl::Helpers::Slack
  end
end

module RoboPigeon::Dsl
  class Root
    def slack(&block)
      RoboPigeon::Dsl::SlackRoot.run(&block)
    end
  end
end

module RoboPigeon::Dsl
  class Job
    def slack(&block)
      RoboPigeon::Dsl::Slack.run(&block)
    end
  end
end
