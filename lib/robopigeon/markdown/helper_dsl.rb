require 'erb'
require 'markdown2confluence'
require 'kramdown/converter/slack'

module RoboPigeon::Dsl
  module Helpers
    module Markdown
      RoboPigeon::Documentarian.add_command(
        'confluence_from_md',
        params: [
          { name: 'file', type: 'String', desc: 'path to a markdown erb file', example: 'config/wiki-page-template.erb.md' }
        ],
        block: ['helpers'],
        desc: 'get jira/confluence markup from an erb markdown template'
      )
      def confluence_from_md(file)
        from_md_template(file).to_confluence
      end

      RoboPigeon::Documentarian.add_command(
        'jira_from_md',
        params: [
          { name: 'file', type: 'String', desc: 'path to a markdown erb file', example: 'config/issue-template.erb.md' }
        ],
        block: ['helpers'],
        desc: 'get jira/confluence markup from an erb markdown template'
      )
      def jira_from_md(file)
        from_md_template(file).to_confluence
      end

      RoboPigeon::Documentarian.add_command(
        'html_from_md',
        params: [
          { name: 'file', type: 'String', desc: 'path to a markdown erb file', example: 'config/results-template.erb.md' }
        ],
        block: ['helpers'],
        desc: 'get html markup from an erb markdown template'
      )
      def html_from_md(file)
        from_md_template(file).to_html
      end

      RoboPigeon::Documentarian.add_command(
        'slack_from_md',
        params: [
          { name: 'file', type: 'String', desc: 'path to a markdown erb file', example: 'config/message-template.erb.md' }
        ],
        block: ['helpers'],
        desc: 'get slack markup from an erb markdown template'
      )
      def slack_from_md(file)
        from_md_template(file).to_slack
      end

      private

      def from_md_template(file)
        raise RoboPigeon::Markdown::Error, "template file #{file} not found" unless File.exist?(file)

        raw = File.read(file)
        markdown = ERB.new(raw).result(binding)
        Kramdown::Document.new(markdown,
                               input: 'GFM',
                               syntax_highlighter: 'coderay',
                               syntax_highlighter_opts: {
                                 css: 'style',
                                 line_numbers: 'table'
                               })
      end
    end
  end
end
