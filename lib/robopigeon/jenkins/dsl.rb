module RoboPigeon::Dsl
  class JenkinsRoot < RoboPigeon::Dsl::Base
    def self.run(&block)
      instance = new
      instance.instance_eval(&block)
    end

    RoboPigeon::Documentarian.add_command(
      'enabled',
      block: ['jenkins'],
      params: [
        { name: 'enabled', type: 'boolean', desc: 'should it run Jenkins commands', example: 'false', default: 'true' }
      ],
      desc: 'Set whether or not to run Jenkins commands, defaults to true'
    )
    def enabled(bool)
      RoboPigeon::Jenkins::Client.enabled = bool
    end

    RoboPigeon::Documentarian.add_command(
      'api_url',
      block: ['jenkins'],
      params: [
        { name: 'url', type: 'String', desc: 'jenkins server url', example: 'https://jenkins.example.com' }
      ],
      desc: 'Set the url for your jenkins server'
    )
    def api_url(url)
      RoboPigeon::Jenkins::Client.url = url
    end

    RoboPigeon::Documentarian.add_command(
      'api_user',
      block: ['jenkins'],
      params: [
        { name: 'user', type: 'String', desc: 'jenkins server user', example: 'pigeon' }
      ],
      desc: 'Set the user for your jenkins server'
    )
    def api_user(user)
      RoboPigeon::Jenkins::Client.user = user
    end

    RoboPigeon::Documentarian.add_command(
      'api_key',
      block: ['jenkins'],
      params: [
        { name: 'token', type: 'String', desc: 'jenkins server token', example: '9eNVDMWuY3zf0DZ5yxLH9Q' }
      ],
      desc: 'Set the token for your jenkins server'
    )
    def api_key(token)
      RoboPigeon::Jenkins::Client.token = token
    end
  end
  class Jenkins < JenkinsRoot
    attr_accessor :job
    def initialize
      self.job = RoboPigeon::Jenkins::Job.new
    end

    def self.run(run_now = true, &block)
      if RoboPigeon::Jenkins::Client.enabled
        instance = new
        instance.instance_eval(&block)
        instance.job.build_and_watch! if run_now
        instance.job
      else
        puts 'Jenkins is disabled, please remove `enabled false` from your global jenkins config'
      end
    end

    RoboPigeon::Documentarian.add_command(
      'param',
      block: ['jenkins'],
      params: [
        { name: 'key', type: 'String', desc: 'the key for the parameter to set', example: 'BUILD_PARAM' },
        { name: 'value', type: 'String', desc: 'the value for the parameter to set', example: '42' }
      ],
      desc: 'Add a parameter to the job'
    )
    def param(key, value)
      job.params[key] = value
    end

    def start_timeout(seconds)
      job.start_timeout = seconds
    end

    def name(name)
      job.name = name
    end

    def completion_timeout(seconds)
      job.build_timeout = seconds
    end
  end
end
