require 'jenkins_api_client'

module RoboPigeon::Jenkins
  class Client
    @enabled = true

    class << self
      attr_accessor :url, :user, :token, :enabled
      def client
        @client ||= JenkinsApi::Client.new(
          server_url: url,
          username: user,
          password: token
        )
      end
    end
  end
end
