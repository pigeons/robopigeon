module RoboPigeon::Jenkins
  class Failure < StandardError; end
  class Job
    DEFAULT_START_TIMEOUT = (45 * 60) # 45 minutes
    attr_accessor :build_timeout, :name, :params, :poll_interval, :build_opts, :logs, :jid
    def initialize
      self.build_timeout = 60 * 60 # 60 minutes in seconds
      self.params = {}
      self.poll_interval = 10
      self.build_opts = {
        'build_start_timeout' => DEFAULT_START_TIMEOUT,
        'cancel_on_build_start_timeout' => true
      }
    end

    def client
      RoboPigeon::Jenkins::Client.client
    end

    def start_timeout=(timeout)
      build_opts['build_start_timeout'] = timeout
    end

    def job_url(jid)
      "#{RoboPigeon::Jenkins::Client.url}/jobs/#{name}/#{jid}"
    end

    def build_and_watch!
      self.jid = client.job.build(name, params.to_h, build_opts)
      puts "Created jenkins job: #{job_url(jid)}"

      watch_build
    end

    private

    def watch_build(iteration=0)
      if iteration * poll_interval > build_timeout
        output_logs(jid)
        raise Timeout::Error, "Build timed out, see logs or look at #{job_url(jid)}" unless job_success?(jid)
      elsif job_complete?(jid)
        output_logs(jid)
        raise RoboPigeon::Jenkins::Failure, "Build did not complete successfully, see logs or look at #{job_url(jid)}" unless job_success?(jid)

        return
      end

      puts "Job not yet complete, Run time: #{(iteration * poll_interval / 60.0).round(2)} minutes"
      sleep poll_interval
      watch_build(iteration + 1)
    end

    def job_complete?(jid)
      return true if client.job.get_build_details(name, jid)['result'] == 'SUCCESS'
      return true if client.job.get_build_details(name, jid)['result'] == 'FAILURE'

      false
    end

    def output_logs(jid)
      self.logs = client.api_get_request("/job/#{name}/#{jid}/consoleText", nil, '')
      puts logs
    end

    def job_success?(jid)
      client.job.get_build_details(name, jid)['result'] == 'SUCCESS'
    end
  end
end
