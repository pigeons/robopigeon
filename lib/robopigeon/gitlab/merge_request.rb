module RoboPigeon::GitLab
  class MergeRequest
    attr_accessor :merge_request
    def initialize(merge_request)
      self.merge_request = merge_request
    end

    def self.create!(source, target, title)
      RoboPigeon::GitLab::MergeRequest.new(RoboPigeon::GitLab::Client.client.create_merge_request(
                                             RoboPigeon::GitLab::Client.project,
                                             title,
                                             source_branch: source,
                                             target_branch: target
                                           ))
    end

    def self.find(source, target)
      RoboPigeon::GitLab::MergeRequest.new(
        RoboPigeon::GitLab::Client.client.merge_requests(
          RoboPigeon::GitLab::Client.project,
          state: 'opened',
          source_branch: source,
          target_branch: target
        ).first
      )
    end

    def gitlab
      RoboPigeon::GitLab::Client
    end

    def client
      gitlab.client
    end

    def comment!(text)
      client.create_merge_request_note(RoboPigeon::GitLab::Client.project, merge_request.iid, body: text)
    end

    def merge!
      return if already_up_to_date

      return if merge_merge_request!

      close_merge_request!
      raise 'Merge request could not be merged'
    end

    private

    def already_up_to_date
      if merge_request.changes_count.nil? || merge_request.changes_count == '0'
        close_merge_request!
        puts 'Merge request had no changes, aborting.'
        return true
      end
      false
    end

    def merge_merge_request!
      begin
        merge = client.accept_merge_request(RoboPigeon::GitLab::Client.project, merge_request.iid, merge_when_pipeline_succeeds: true)
        return merge.merge_status == 'can_be_merged' && (merge.merge_when_pipeline_succeeds || merge.state == 'merged')
      rescue Gitlab::Error::Error => e
        puts "Failed to merge branch - #{e}\n#{e.message}"
      end
      false
    end

    def close_merge_request!
      client.update_merge_request(RoboPigeon::GitLab::Client.project, merge_request.iid, state_event: 'close')
      true
    rescue Gitlab::Error::Error => e
      puts "Failed to close merge request - #{e}\n#{e.message}"
      false
    end
  end
end
