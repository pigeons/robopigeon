module RoboPigeon::GitLab
  class Commit
    attr_accessor :files, :branch, :message, :author_name, :author_email
    def initialize
      self.files = []
      self.author_name = ENV['GITLAB_USER_NAME']
      self.author_email = ENV['GITLAB_USER_EMAIL']
      self.branch = RoboPigeon::GitLab::Client.branch
    end

    def add_file(file, content=nil, method='update')
      content ||= File.read(file)
      files.push(action: method, file_path: file, content: content)
    end

    def client
      RoboPigeon::GitLab::Client.client
    end

    def commit!
      client.create_commit(
        RoboPigeon::GitLab::Client.project,
        branch,
        message,
        files,
        author_name: author_name,
        author_email: author_email
      )
    end
  end
end
