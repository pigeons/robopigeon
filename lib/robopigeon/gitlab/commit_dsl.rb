require 'gitlab'
module RoboPigeon::Dsl
  class GitLabCommit < RoboPigeon::Dsl::Base
    attr_accessor :commit
    def initialize
      self.commit = RoboPigeon::GitLab::Commit.new
    end

    def self.run(&block)
      instance = new
      instance.instance_eval(&block)
      instance.commit.commit!
      instance.commit
    end

    RoboPigeon::Documentarian.add_command(
      'file',
      block: %w[job gitlab commit],
      params: [
        { name: 'path', type: 'String', desc: 'repo path of the file to add', example: 'app/config.rb' },
        { name: 'contents', type: 'Text', desc: 'the content of the file to add (optional)', example: 'here is the contents of my file', default: 'on disk contents of file' }
      ],
      desc: 'add a file to the commit'
    )
    def file(path, content=nil)
      method = File.exist?(path) ? 'update' : 'create'
      commit.add_file(path, content, method)
    end

    RoboPigeon::Documentarian.add_command(
      'file',
      block: %w[job gitlab commit],
      params: [
        { name: 'snapshot', type: 'Boolean', desc: 'add snapshot flag', example: 'false', default: 'true' }
      ],
      desc: 'add all of the version files to the commit'
    )
    def version_files(snapshot: false)
      RoboPigeon::Versioner::Version.add_all_to_commit(commit, snapshot)
    end

    RoboPigeon::Documentarian.add_command(
      'message',
      block: %w[job gitlab commit],
      params: [
        { name: 'message', type: 'string', desc: 'message to use in the commit', example: 'A commit to end all commits!' }
      ],
      desc: 'Set the message for the commit'
    )
    def message(message)
      commit.message = message
    end

    RoboPigeon::Documentarian.add_command(
      'author',
      block: %w[job gitlab commit],
      params: [
        { name: 'name', type: 'String', desc: 'name of the user who is making the commit', example: 'Robo Pigeon', default: "ENV['GITLAB_USER_NAME']" },
        { name: 'email', type: 'String', desc: 'email of the user who is making the commit', example: 'robo.pigeon@example.com', default: "ENV['GITLAB_USER_EMAIL']" }
      ],
      desc: 'set the author name and email address.'
    )
    def author(name, email)
      commit.author_email = email
      commit.author_name = name
    end

    RoboPigeon::Documentarian.add_command(
      'branch',
      block: %w[job gitlab commit],
      params: [
        { name: 'name', type: 'String', desc: 'name of the branch to commit to', example: 'master', defautl: "ENV['CI_COMMIT_REF_NAME']" }
      ],
      desc: 'Set the branch to commit to'
    )
    def branch(name)
      commit.branch = name
    end
  end
end
