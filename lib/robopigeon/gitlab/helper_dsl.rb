require 'gitlab'

module RoboPigeon::Dsl
  module Helpers
    module GitLab
      RoboPigeon::Documentarian.add_command('deployment_sha', block: ['helpers'], params: [{ name: 'environment', type: 'String', desc: 'the name of a gitlab operations environment', example: 'production' }], desc: 'get the git sha of the specified deployed environement')
      def deployment_sha(environment)
        dep = RoboPigeon::GitLab::Client.get_deployment(environment)
        dep.sha
      end

      RoboPigeon::Documentarian.add_command('deployment_ref', block: ['helpers'], params: [{ name: 'environment', type: 'String', desc: 'the name of a gitlab operations environment', example: 'production' }], desc: 'get the git ref (branch or tag) of the specified deployed environement')
      def deployment_ref(environment)
        dep = RoboPigeon::GitLab::Client.get_deployment(environment)
        dep.ref
      end

      RoboPigeon::Documentarian.add_command('deployment_diff_link', block: ['helpers'], params: [{ name: 'environment', type: 'String', desc: 'the name of a gitlab operations environment', example: 'production' }], desc: 'get a link to the changes between the last deploy and the current ref')
      def deployment_diff_link(environment)
        dep = RoboPigeon::GitLab::Client.get_deployment(environment)
        "#{ENV['CI_PROJECT_URL']}/compare/#{dep.ref}...#{ENV['CI_COMMIT_TAG']}"
      end

      RoboPigeon::Documentarian.add_command('deployment_shortlog', block: ['helpers'], params: [{ name: 'environment', type: 'String', desc: 'the name of a gitlab operations environment', example: 'production' }], desc: 'get a shortlog of changes between head and the specified environment')
      def deployment_shortlog(environment)
        dep = RoboPigeon::GitLab::Client.get_deployment(environment)
        `git shortlog --no-merges #{dep.sha}..`.strip
      end

      RoboPigeon::Documentarian.add_command('deployment_time', block: ['helpers'], params: [{ name: 'environment', type: 'String', desc: 'the name of a gitlab operations environment', example: 'production' }], desc: 'get the time the last deployment for the given environment happened')
      def deployment_time(environment)
        dep = RoboPigeon::GitLab::Client.get_deployment(environment)
        dep.deployable.finished_at
      end

      RoboPigeon::Documentarian.add_command('environment_link', block: ['helpers'], params: [{ name: 'environment', type: 'String', desc: 'the name of a gitlab operations environment', example: 'production' }], desc: 'get a link to the environment by name')
      def environment_link(environment)
        dep = RoboPigeon::GitLab::Client.get_deployment(environment)
        "#{ENV['CI_PROJECT_URL']}/environments/#{dep.environment.id}"
      end

      RoboPigeon::Documentarian.add_command('deployment_code_change_stats', block: ['helpers'], params: [{ name: 'environment', type: 'String', desc: 'the name of a gitlab operations environment', example: 'production' }], desc: 'Get code change stats since the latest deployment')
      def deployment_code_change_stats(environment)
        dep = RoboPigeon::GitLab::Client.get_deployment(environment)
        `git diff --shortstat #{dep.sha}`.strip
      end

      RoboPigeon::Documentarian.add_command(
        'deployment_git_log_jira_tickets',
        block: ['helpers'],
        params: [
          { name: 'environment', type: 'String', desc: 'the name of a gitlab operations environment', example: 'production' },
          { name: 'project_key', type: 'String', desc: 'the project key to search history for', example: 'TIC' }
        ],
        desc: 'returns a list of jira tickets for the given project key in history since the deployment'
      )
      def tickets_in_log_since_deployment_to(environment, matcher=/[A-Za-z]+-\d+/)
        dep = RoboPigeon::GitLab::Client.get_deployment(environment)
        log = `git log #{dep.sha}..`
        log.scan(matcher)
      end
    end
  end
end
