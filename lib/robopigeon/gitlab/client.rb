module RoboPigeon::GitLab
  class Client
    @project = ENV['CI_PROJECT_PATH']
    @pipeline_id = ENV['CI_PIPELINE_ID']
    @branch = ENV['CI_COMMIT_REF_NAME']
    @enabled = true

    class << self
      attr_accessor :project, :branch, :pipeline_id, :enabled
      def client
        Gitlab.client
      end

      def get_deployment(environment, page = 0)
        deployments = client.deployments(
          project,
          order_by: 'created_at',
          sort: 'desc',
          per_page: 20,
          page: page
        )
        deployment = deployments.select { |dep| dep.environment.name == environment }.first
        return get_deployment(environment, page + 1) if deployment.nil? && deployments.has_next_page?

        deployment
      end

      def create_tag(ref, tag, message)
        client.create_tag(project, tag, ref, message)
      end

      def merge_request_comment(text, source = branch)
        merge_requests = client.merge_requests(project, source_branch: source)
        raise "No merge requests exist for branch '#{source}'" if merge_requests.empty?

        merge_requests.each do |merge_request|
          client.create_merge_request_discussion(project, merge_request.iid, body: text)
        end
      end
    end
  end
end
