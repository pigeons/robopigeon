require 'gitlab'
module RoboPigeon::Dsl
  class GitLabRoot < RoboPigeon::Dsl::Base
    RoboPigeon::Documentarian.add_command(
      'enabled',
      block: ['gitlab'],
      params: [
        { name: 'enabled', type: 'boolean', desc: 'should it run gitlab commands', example: 'false', default: 'true' }
      ],
      desc: 'Set whether or not to run gitlab commands, defaults to true'
    )
    def enabled(bool)
      RoboPigeon::GitLab::Client.enabled = bool
    end

    RoboPigeon::Documentarian.add_command(
      'api_key',
      block: ['gitlab'],
      params: [
        { name: 'api_key', type: 'String', desc: 'your gitlab api key', example: 'aeoiabDasdf-B', default: "ENV['GITLAB_API_KEY']" }
      ],
      desc: 'Set the gitlab api key for your gitlab account'
    )
    def api_key(key)
      Gitlab.private_token = key
    end

    RoboPigeon::Documentarian.add_command(
      'api_url',
      block: ['gitlab'],
      params: [
        { name: 'url', type: 'String', desc: 'the url to your gitlab instance', example: 'https://gitlab.com/api/v4/', default: "ENV['CI_API_V4_URL']" }
      ],
      desc: 'Set the gitlab api url for your account'
    )
    def api_url(url)
      Gitlab.endpoint = url
    end

    RoboPigeon::Documentarian.add_command(
      'project',
      block: ['gitlab'],
      params: [
        { name: 'project_path', type: 'String', desc: 'path to your project', example: 'robopigeon/robopigeon', default: "ENV['CI_PROJECT_PATH']" }
      ],
      desc: 'Override the project path reference'
    )
    def project(project_path)
      RoboPigeon::GitLab::Client.project = project_path
    end

    RoboPigeon::Documentarian.add_command(
      'pipeline_id',
      block: ['gitlab'],
      params: [
        { name: 'pipeline_id', type: 'String', desc: 'id of the current pipeline', example: '1359234', default: "ENV['CI_PIPELINE_ID']" }
      ],
      desc: 'Override the pipeline id reference'
    )
    def pipeline_id(pipeline_id)
      RoboPigeon::GitLab::Client.pipeline_id = pipeline_id
    end

    RoboPigeon::Documentarian.add_command(
      'branch',
      block: ['gitlab'],
      params: [
        { name: 'branch', type: 'String', desc: 'branch you want to refer to', example: 'master', default: "ENV['CI_COMMIT_REF_NAME']" }
      ],
      desc: 'Override the branch reference'
    )
    def branch(branch)
      RoboPigeon::GitLab::Client.branch = branch
    end
  end

  class GitLab < GitLabRoot
    attr_accessor :merge_request

    def self.run(&block)
      if RoboPigeon::GitLab::Client.enabled
        gitlab = RoboPigeon::Dsl::GitLab.new
        gitlab.instance_eval(&block)
      else
        puts 'Gitlab is disabled, please remove `enabled false` from your global gitlab config'
      end
    end

    RoboPigeon::Documentarian.add_command(
      'merge_request_comment',
      block: %w[job gitlab],
      params: [
        { name: 'text', type: 'String', desc: 'text you want to post as a comment', example: 'this branch failed during a deployment' },
        { name: 'source_branch', type: 'String', desc: 'branch you want to refer to', example: 'master', default: "current global branch, defaults ENV['CI_COMMIT_REF_NAME']" }
      ],
      desc: 'post a comment to all merge requests with the branch source'
    )
    def merge_request_comment(text, source_branch = RoboPigeon::GitLab::Client.branch)
      RoboPigeon::GitLab::Client.merge_request_comment(text, source_branch)
    end

    RoboPigeon::Documentarian.add_command(
      'create_merge_request',
      block: %w[job gitlab],
      params: [
        { name: 'source_branch', type: 'String', desc: 'source branch you want to merge from', example: "ENV['CI_COMMIT_REF_NAME']" },
        { name: 'target_branch', type: 'String', desc: 'target branch to merge to', example: 'master' },
        { name: 'title', type: 'String', desc: 'Optional string to use as the merge request title.', example: 'Merge my_face into master on tuesdays', default: "Automated merge request from \#{source} to \#{target}" }
      ],
      desc: 'create a merge request between two supplied branches'
    )
    def create_merge_request(source, target, arg_title=nil)
      title = arg_title || "Automated merge request from #{source} to #{target}"
      self.merge_request = RoboPigeon::GitLab::MergeRequest.create!(source, target, title)
    end

    RoboPigeon::Documentarian.add_command(
      'merge_branches',
      block: %w[job gitlab],
      params: [
        { name: 'source_branch', type: 'String', desc: 'source branch you want to merge from', example: "ENV['CI_COMMIT_REF_NAME']" },
        { name: 'target_branch', type: 'String', desc: 'target branch to merge to', example: 'master' },
        { name: 'title', type: 'String', desc: 'Optional string to use as the merge request title.', example: 'Merge my_face into master on tuesdays', default: "Automated merge request from \#{source} to \#{target}" }
      ],
      desc: 'merge two branches together using a merge request that automatically merges'
    )
    def merge_branches(source, target, arg_title=nil)
      title = arg_title || "Automated merge request from #{source} to #{target}"
      RoboPigeon::GitLab::MergeRequest.create!(source, target, title).merge!
    end

    RoboPigeon::Documentarian.add_command(
      'tag',
      block: %w[job gitlab],
      params: [
        { name: 'tag', type: 'String', desc: 'the tag to create', example: '1.2.3' },
        { name: 'ref', type: 'String', desc: 'branch or sha that you want to tag', example: 'master', default: ENV['CI_COMMIT_REF_NAME'] },
        { name: 'message', type: 'String', desc: 'message to annotate the tag with', example: 'updating version to 1.2.3', default: "RoboPigeon tagged at \#{Time.now}" }
      ],
      desc: 'look up a merge request by source and target'
    )
    def tag(tag, ref=ENV['CI_COMMIT_REF_NAME'], message="RoboPigeon tagged at #{Time.now}")
      RoboPigeon::GitLab::Client.create_tag(ref, tag, message)
    end

    RoboPigeon::Documentarian.add_command(
      'find_merge_request',
      block: %w[job gitlab],
      params: [
        { name: 'source_branch', type: 'String', desc: 'source branch you want to merge from', example: "ENV['CI_COMMIT_REF_NAME']" },
        { name: 'target_branch', type: 'String', desc: 'target branch to merge to', example: 'master' }
      ],
      desc: 'look up a merge request by source and target'
    )
    def find_merge_request(source, target)
      RoboPigeon::GitLab::MergeRequest.find(source, target)
    end

    RoboPigeon::Documentarian.add_command(
      'merge_merge_request',
      block: %w[job gitlab],
      desc: 'merge the recently created merge request'
    )
    def merge_merge_request
      merge_request.merge!
    end

    RoboPigeon::Documentarian.add_block(
      'commit',
      helpers: true,
      block: %w[job gitlab],
      desc: 'Create a commit on a branch'
    )
    def commit(&block)
      RoboPigeon::GitLab::Commit.run(&block)
    end
  end
end
