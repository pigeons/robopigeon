module RoboPigeon::Dsl
  module Helpers
    module Jira
      RoboPigeon::Documentarian.add_command('jira_last_created_ticket', block: ['helpers'], desc: 'Returns the id of the last created ticket.')
      def jira_last_created_ticket
        RoboPigeon::Jira::Client.last_created_ticket
      end

      RoboPigeon::Documentarian.add_command('jira_last_created_ticket_link', block: ['helpers'], desc: 'Returns a link to the last created ticket.')
      def jira_last_created_ticket_link
        issue_id = RoboPigeon::Jira::Client.last_created_ticket
        jira_url = RoboPigeon::Jira::Client.api_url
        "#{jira_url}/browse/#{issue_id}"
      end

      RoboPigeon::Documentarian.add_command('jira_last_created_ticket_slack_link', block: ['helpers'], desc: 'Returns a slack formatted link to the last created ticket.')
      def jira_last_created_ticket_slack_link
        issue_id = RoboPigeon::Jira::Client.last_created_ticket
        jira_url = RoboPigeon::Jira::Client.api_url
        "<#{jira_url}/browse/#{issue_id}|#{issue_id}>"
      end

      RoboPigeon::Documentarian.add_command(
        'jira_issue_slack_link',
        params: [
          { name: 'issue_id', type: 'String', desc: 'Issue to link to', example: 'TICK-1234' }
        ],
        block: ['helpers'],
        desc: 'Returns a slack formatted link to the last created ticket.'
      )
      def jira_slack_link(issue_id)
        jira_url = RoboPigeon::Jira::Client.api_url
        "<#{issue_id}|#{jira_url}/browse/#{issue_id}>"
      end
    end
  end
end
