module RoboPigeon::Dsl
  class JiraTicket < RoboPigeon::Dsl::Base
    include RoboPigeon::Dsl::Helpers
    attr_accessor :ticket

    def self.run(ticket_number = nil, &block)
      ticket = RoboPigeon::Jira::Ticket.new(ticket_number)
      jira = RoboPigeon::Dsl::JiraTicket.new
      jira.ticket = ticket
      jira.instance_eval(&block)
      jira.ticket.create! unless jira.ticket.ticket
      ticket
    end

    RoboPigeon::Documentarian.add_command(
      'assign',
      block: %w[job jira ticket],
      params: [
        { name: 'email', type: 'String', desc: 'the email address for the person to assign the ticket to', example: 'someone@example.com' }
      ],
      desc: 'assign the ticket to a person by email address'
    )
    def assign(email)
      ticket.assign(email)
    end

    RoboPigeon::Documentarian.add_command(
      'reporter',
      block: %w[job jira ticket],
      params: [
        { name: 'email', type: 'String', desc: 'the email address for the person to reporter the ticket to', example: 'someone@example.com' }
      ],
      desc: 'reporter the ticket to a person by email address'
    )
    def reporter(email)
      ticket.set_reporter(email)
    end

    RoboPigeon::Documentarian.add_command(
      'comment',
      block: %w[job jira ticket],
      params: [
        { name: 'comment', type: 'String', desc: 'the comment to add to the ticket', example: 'A string comment' }
      ],
      desc: 'add a comment to the ticket'
    )
    def comment(comment)
      ticket.add_comment(comment)
    end

    RoboPigeon::Documentarian.add_command(
      'project',
      block: %w[job jira ticket],
      params: [
        { name: 'project_key', type: 'String', desc: 'the key for the project you want to create the ticket in', example: 'TIC' }
      ],
      desc: 'set which project you want to create the ticket in (only used when creating)'
    )
    def project(project_key)
      ticket.project = project_key
    end

    RoboPigeon::Documentarian.add_command(
      'summary',
      block: %w[job jira ticket],
      params: [
        { name: 'title', type: 'String', desc: 'title summary for your issue', example: 'A Bug has happend!' }
      ],
      desc: 'set the issue summary (only used in creation, use the field funtion for updates)'
    )
    def summary(title)
      ticket.summary = title
    end

    RoboPigeon::Documentarian.add_command(
      'description',
      block: %w[job jira ticket],
      params: [
        { name: 'text', type: 'String', desc: 'the main body of a new issue', example: 'Lots of content in here' }
      ],
      desc: 'Set the main body of the issue here, only used in creation, use the field function to update'
    )
    def description(text)
      ticket.description = text
    end

    RoboPigeon::Documentarian.add_command(
      'issuetype',
      block: %w[job jira ticket],
      params: [
        { name: 'type', type: 'String', desc: 'name of the issue type', example: 'Story' }
      ],
      desc: 'sets the issue type to use when creating a ticket'
    )
    def issuetype(type)
      ticket.set_issuetype type
    end

    RoboPigeon::Documentarian.add_command(
      'create!',
      block: %w[job jira ticket],
      desc: 'creates the configured ticket and returns the id'
    )
    def create!
      ticket.create!
      ticket.ticket
    end

    RoboPigeon::Documentarian.add_command(
      'field',
      block: %w[job jira ticket],
      params: [
        { name: 'field_name', type: 'String', desc: 'the name of the field you want to set', example: 'Regression Test Release Date' },
        { name: 'field_value', type: 'String', desc: 'what to set that field to', example: '2/10/2030' }
      ],
      desc: 'set or update a field to a value'
    )
    def field(name, value)
      ticket.set_field(name, value)
    end

    RoboPigeon::Documentarian.add_command(
      'print_id',
      block: %w[job jira ticket],
      desc: 'print the ticket number to standard out (only for already created tickets)'
    )
    def print_id
      puts ticket.ticket.to_s
    end

    RoboPigeon::Documentarian.add_command(
      'transition',
      block: %w[job jira ticket],
      params: [
        { name: 'transition_name', type: 'String', desc: 'the name of the transition to run', example: 'Ready' }
      ],
      desc: 'transition a ticket to a new state'
    )
    def transition(name)
      ticket.perform_transition(name)
    end

    RoboPigeon::Documentarian.add_command(
      'wait_for_state',
      block: %w[job jira ticket],
      params: [
        { name: 'state_name', type: 'String', desc: 'the name of the state to wait for', example: 'Approved' }
      ],
      desc: 'block execution waiting for a state transition'
    )
    def wait_for_state(transition_state)
      ticket.wait_for_state!(transition_state)
    end
  end
end
