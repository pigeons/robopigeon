module RoboPigeon::Dsl
  class JiraRoot < RoboPigeon::Dsl::Base
    RoboPigeon::Documentarian.add_command(
      'enabled',
      block: ['jira'],
      params: [
        { name: 'enabled', type: 'boolean', desc: 'should it run jira commands', example: 'false', default: 'true' }
      ],
      desc: 'Set whether or not to run jira commands, defaults to true'
    )
    def enabled(bool)
      RoboPigeon::Jira::Client.enabled = bool
    end

    RoboPigeon::Documentarian.add_command(
      'api_key',
      block: ['jira'],
      params: [
        { name: 'api_key', type: 'String', desc: 'the api key for your jira account', example: 'someLongString12ofLettersAndNumber5', default: "ENV['JIRA_TOKEN']" }
      ],
      desc: 'set your jira api key globally'
    )
    def api_key(key)
      RoboPigeon::Jira::Client.api_key = key
    end

    RoboPigeon::Documentarian.add_command(
      'api_url',
      block: ['jira'],
      params: [
        { name: 'api_url', type: 'String', desc: 'the api url for your jira', example: 'https://jira.example.com/' }
      ],
      desc: 'set your jira api key globally'
    )
    def api_url(key)
      RoboPigeon::Jira::Client.api_url = key
    end

    RoboPigeon::Documentarian.add_command(
      'wait_for_state_timeout',
      block: ['jira'],
      params: [
        { name: 'time', type: 'String', desc: 'how long to wait for a state before you time out', example: '1.hour', default: '10.minutes' }
      ],
      desc: 'set timeout for how long RoboPigeon will wait for designated state'
    )
    def wait_for_state_timeout(time)
      RoboPigeon::Jira::Client.wait_for_state_timeout = time
    end
  end

  class Jira < JiraRoot
    def self.run(&block)
      if RoboPigeon::Jira::Client.enabled
        jira = RoboPigeon::Dsl::Jira.new
        jira.instance_eval(&block)
      else
        puts 'Jira is disabled, please remove `enabled false` from your global jira config'
      end
    end

    RoboPigeon::Documentarian.add_block(
      'ticket',
      params: [
        { name: 'number', type: 'String', desc: 'Existing ticket number to reference (optional)', example: 'TIC-1234' }
      ],
      helpers: true,
      block: %w[job jira],
      desc: 'Create or update a ticket'
    )
    def ticket(number = nil, &block)
      RoboPigeon::Dsl::JiraTicket.run(number, &block)
    end

    RoboPigeon::Documentarian.add_block(
      'comment_on',
      params: [
        { name: 'number', type: 'String', desc: 'Existing ticket number to reference', example: 'TIC-1234' },
        { name: 'comment', type: 'String', desc: 'Comment to add to the ticket', example: 'A comment for a ticket' }
      ],
      helpers: true,
      block: %w[job jira],
      desc: 'Add a comment to specified ticket'
    )
    def comment_on(number, comment)
      jira = RoboPigeon::Jira::Ticket.new(number)
      jira.add_comment(comment)
    end
  end
end
