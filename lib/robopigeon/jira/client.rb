module RoboPigeon::Jira
  class Client
    @enabled = true
    @api_key = ENV['JIRA_TOKEN']
    @last_created_ticket = File.read('last_created_jira_ticket') if File.exist?('last_created_jira_ticket')
    @wait_for_state_timeout = 10.minutes

    class << self
      attr_accessor :api_key, :api_url, :enabled, :last_created_ticket, :wait_for_state_timeout
      def jira_request
        Faraday.new(url: RoboPigeon::Jira::Client.api_url) do |faraday|
          faraday.adapter Faraday.default_adapter
          faraday.headers['Content-Type'] = 'application/json'
          faraday.headers['Authorization'] = 'Basic ' + RoboPigeon::Jira::Client.api_key
        end
      end
    end
  end
end
