require 'json'

module RoboPigeon
  module Documentarian
    @@documents = {}

    def self.add_block(name, **options)
      block_desc = options[:desc]
      helpers = options[:helpers] || false
      document = get_document(block: options[:block], document: @@documents)
      document[name] = {
        block_desc: block_desc,
        includes_helpers: helpers
      }
      document[name][:params] = options[:params] if options[:params]
    end

    def self.add_command(name, **options)
      command_desc = options[:desc]
      document = get_document(block: options[:block], document: @@documents)
      document[name] = {
        command_desc: command_desc
      }
      document[name][:params] = options[:params] if options[:params]
    end

    def self.generate_docs
      JSON.pretty_generate(@@documents)
    end

    def self.get_document(**options)
      block = options[:block]
      document = options[:document]
      return get_document(document: document[block.shift], block: block) unless block.empty?

      document
    end
  end
end
