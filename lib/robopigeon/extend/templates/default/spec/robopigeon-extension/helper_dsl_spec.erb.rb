require 'spec_helper'

describe RoboPigeon::Dsl::Helpers::<%= extension_name_upper %> do
  class <%= extension_name_upper %>HelperTest
    include RoboPigeon::Dsl::Helpers::<%= extension_name_upper %>
  end
  subject { <%= extension_name_upper %>HelperTest.new }

  context 'some_helper_dsl' do
    it 'does something really cool!' do
      expect(subject.some_helper_dsl).to eq('nothing happens here yet')
    end
  end
end
