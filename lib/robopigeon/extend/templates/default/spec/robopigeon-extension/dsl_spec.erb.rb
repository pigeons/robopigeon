require 'spec_helper'

describe RoboPigeon::Dsl::GitLabRoot do
  subject { RoboPigeon::Dsl::Root }

  context 'do_some_global_thing' do
    it 'does some global thing' do
      expect do
        subject.run do
          <%= extension_name_lower %> do
            do_some_global_thing
          end
        end
      end.not_to raise_error
    end
  end
end

describe RoboPigeon::Dsl::GitLab do
  subject { RoboPigeon::Dsl::Job }

  context 'do_something' do
    it 'does some global thing' do
      expect do
        subject.run do
          <%= extension_name_lower %> do
            do_something
          end
        end
      end.not_to raise_error
    end
  end
end
