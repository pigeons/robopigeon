$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'bundler/setup'
require 'robopigeon'
require 'robopigeon_<%= extension_name_lower %>'
require 'pry'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

def file_fixture(path)
  File.join(Dir.pwd, 'spec', 'fixtures', path)
end
