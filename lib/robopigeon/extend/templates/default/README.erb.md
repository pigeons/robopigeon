# RoboPigeon <%= extension_name_upper %>

TODO: Add a description of what dsls/helpers this adds.

## Installation

This Gem is developed against ruby 2.5, but is tested in CI against the latest versions of `2.1`, `2.3`, and `2.5`

Add this line to your application's Gemfile:

```ruby
gem 'robopigeon_<%= extension_name_lower %>'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install robopigeon_<%= extension_name_lower %>

## Usage

TODO: Add usage instructions for your new DSLs

## Development

After checking out the repo, run `bundle install`

Then run `bundle exec rspec` to run the tests.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/{gitlab_username}/robopigeon_<%= extension_name_lower %>.
