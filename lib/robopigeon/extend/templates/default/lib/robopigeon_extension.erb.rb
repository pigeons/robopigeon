require 'robopigeon_<%= extension_name_lower %>/helper_dsl'
require 'robopigeon_<%= extension_name_lower %>/dsl'
require 'robopigeon_<%= extension_name_lower %>/version'

module RoboPigeon::Dsl
  module Helpers
    include RoboPigeon::Dsl::Helpers::<%= extension_name_upper %>
  end
end

module RoboPigeon::Dsl
  class Root
    def <%= extension_name_lower %>(&block)
      RoboPigeon::Dsl::<%= extension_name_upper %>Root.run(&block)
    end
  end
end

module RoboPigeon::Dsl
  class Job
    def <%= extension_name_lower %>(&block)
      RoboPigeon::Dsl::<%= extension_name_upper %>.run(&block)
    end
  end
end
