module RoboPigeon::Dsl::Helpers
  module <%= extension_name_upper %>
    # This file contains methods that are added to all levels of the DSL, you can
    # add new ones of your own in here! For example, if you wanted something that
    # holds a specific slack subteam name, you could do something like:
    # def slack_approvers_mention
    #   "<!subteam^A1342B>"
    # end
    # Then you could reference it in a slack notification:
    # slack do
    #   channel "#request-approval"
    #   message "#{slack_approvers_mention} - I would like approval for my life!"
    # end
    def some_helper_dsl
      'nothing happens here yet'
    end
  end
end
