module RoboPigeon::Dsl
  class <%= extension_name_upper %>Root < RoboPigeon::Dsl::Base
    # This is where you include your top level dsls used for configuring
    # global settings like api keys or api urls. In general, it is useful
    # to also include them in the individual job settings in case a job
    # needs to override them.
    # For example, you might set your api key for a service to talk to:
    # def api_key(key)
    #   <%= extension_name_upper %>::Client.api_key = key
    # end
    def do_some_global_thing
      'some global thing'
    end
  end

  class <%= extension_name_upper %> < <%= extension_name_upper %>Root
    # This is where you include job specific dsls! This is used for actions
    # that only take place when running a job, this could be something like
    # pre-configured slack notifications or more complex things.
    # For example, if you wanted a dsl that always sends a slack message to
    # a certain channel:
    # def request_approval(description, ticket_number)
    #   slack do
    #     channel '#approval-requests'
    #     message "#{slack_approvers_mention}: #{slack_user_for ENV['GITLAB_USER_EMAIL'] would like approval for #{jira_slack_link} - #{description}"
    #   end
    # end

    # Alternately, you can add whole new services, like say you wanted an
    # alert in your alerting software, you could write code to trigger it
    # in here!
    # def trigger_alert(description, team)
    #   # Code goes here to trigger the alert
    # end
    def do_something
      'nothing happens yet...'
    end
  end
end
