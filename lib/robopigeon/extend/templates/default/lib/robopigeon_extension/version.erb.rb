module RoboPigeon
  module <%= extension_name_upper %>
    VERSION = File.read(File.join(__dir__, '../../.version')).freeze
  end
end
