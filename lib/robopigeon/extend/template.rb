require 'fileutils'
require 'erb'

module RoboPigeon::Extensions
  class Template
    TEMPLATE_PATH = File.expand_path('../templates/', __FILE__).freeze
    attr_accessor :name, :template

    def self.render(name, path, template='default')
      to_render = new
      to_render.name = name
      to_render.template = template
      raise "No template with name #{template}" unless File.directory?("#{TEMPLATE_PATH}/#{template}")

      to_render.render_to(path)
    end

    def render_to(path)
      cwd = FileUtils.pwd
      base = File.expand_path("#{path}/robopigeon_#{name}")
      FileUtils.cd "#{TEMPLATE_PATH}/#{template}"
      FileUtils.mkdir_p base.to_s
      Dir.glob('{**{,/*/**},.*}') do |file|
        new_filename = file.gsub('extension', name).gsub('.erb', '')
        if File.directory?(file)
          FileUtils.mkdir("#{base}/#{new_filename}") unless File.exist? "#{base}/#{new_filename}"
          puts "Created directory #{base}/#{new_filename}"
        else
          content = ERB.new(File.read(file)).result(binding)
          File.write("#{base}/#{new_filename}", content)
          puts "Created #{base}/#{new_filename}"
        end
      end
      FileUtils.cd cwd
    end

    def extension_name_lower
      name.downcase
    end

    def extension_name_upper
      name.capitalize
    end

    def user_email
      `git config user.email`.strip
    end

    def user_name
      `git config user.name`.strip
    end
  end
end
