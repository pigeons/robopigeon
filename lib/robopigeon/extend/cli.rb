require 'optparse'

module RoboPigeon::Extensions
  class Cli
    class << self
      def invoke(args)
        template = 'default'
        path = FileUtils.pwd
        optparse = OptionParser.new do |opts|
          opts.banner = 'Usage: robopigeon new [options] name'

          opts.on('-d', '--default', 'use the default template') do
            template = 'default'
          end

          opts.on('-h', '--help', 'Show help message') do
            puts opts
            exit 1
          end

          opts.on('-p', '--path [path]', 'path to where the gem should be created') do |selected_path|
            path = selected_path
          end
        end

        args.shift
        options = optparse.parse(args)
        name = options.shift

        RoboPigeon::Extensions::Template.render(name, path, template)
      end
    end
  end
end
