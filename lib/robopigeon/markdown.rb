require 'robopigeon/markdown/helper_dsl'

module RoboPigeon::Markdown
  class Error < StandardError; end
end

module RoboPigeon::Dsl
  module Helpers
    include RoboPigeon::Dsl::Helpers::Markdown
  end
end
