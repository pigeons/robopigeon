RoboPigeon::Documentarian.add_block('gitlab', helpers: true, block: [], desc: 'a configuration block for gitlab')
RoboPigeon::Documentarian.add_block('gitlab', helpers: true, block: ['job'], desc: 'interact with gitlab, can have multiple')

require 'robopigeon/gitlab/dsl'
require 'robopigeon/gitlab/client'
require 'robopigeon/gitlab/commit'
require 'robopigeon/gitlab/merge_request'
require 'robopigeon/gitlab/helper_dsl'
require 'robopigeon/gitlab/commit_dsl'

module RoboPigeon::Dsl
  module Helpers
    include RoboPigeon::Dsl::Helpers::GitLab
  end
end

module RoboPigeon::Dsl
  class Root
    def gitlab(&block)
      RoboPigeon::Dsl::GitLabRoot.run(&block)
    end
  end
end

module RoboPigeon::Dsl
  class Job
    def gitlab(&block)
      RoboPigeon::Dsl::GitLab.run(&block)
    end
  end
end
