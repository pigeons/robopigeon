require 'robopigeon/documentarian'
require 'robopigeon/dsl'
require 'robopigeon/extend'
require 'robopigeon/git'
require 'robopigeon/gitlab'
require 'robopigeon/jenkins'
require 'robopigeon/slack'
require 'robopigeon/jira'
require 'robopigeon/markdown'
require 'robopigeon/version'
require 'robopigeon/versioner'

module RoboPigeon
  DEFAULT_FILE = "#{FileUtils.pwd}/robopigeon.rb".freeze
  class SkippedJob < StandardError; end
end
