require 'spec_helper'
require 'robopigeon'

describe 'robopigeon.rb file' do
  subject { RoboPigeon::Dsl::Root.new }
  context 'version' do
    it 'should print the verison to standard out' do
      expect { subject.run('version') }.to output(RoboPigeon::VERSION).to_stdout
    end
  end
end
