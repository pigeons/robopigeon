slack do
  api_key 'abc1234'
  name 'Automated Pigeon Bot'
  emoji ':pigeon-bot:'
end

job 'on failure' do
  slack do
    channel '#team-pigeon'
    message "Pipeline from #{slack_user_for ENV['GITLAB_USER_EMAIL']} has failed!"
  end

  slack do
    user ENV['GITLAB_USER_EMAIL']
    message 'Your CI Pipeline from has failed!'
  end
end
