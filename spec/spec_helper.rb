require 'bundler/setup'
require 'simplecov'
require 'pry'
require 'webmock/rspec'
require 'vcr'

SimpleCov.start do
  add_filter '/spec/'
end
SimpleCov.minimum_coverage 90

$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'robopigeon'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

VCR.configure do |config|
  config.cassette_library_dir = 'spec/fixtures/vcr_cassettes'
  config.hook_into :webmock
  config.filter_sensitive_data('<JIRA_TOKEN>') { ENV['JIRA_TOKEN'] }
  config.filter_sensitive_data('<GITLAB_API_PRIVATE_TOKEN>') { ENV['GITLAB_API_PRIVATE_TOKEN'] }
  config.filter_sensitive_data('<SLACK_API_KEY>') { ENV['SLACK_API_KEY'] }
  config.filter_sensitive_data('<JENKINS_TOKEN>') { ENV['JENKINS_TOKEN'] }
  config.filter_sensitive_data('<JENKINS_USER>') { ENV['JENKINS_USER'] }
  config.filter_sensitive_data('http://jenkins.example.com/') { ENV['JENKINS_URL'] }

  config.before_record do |interaction|
    # Filter out Cookies and all X- headers
    interaction.response.headers.delete('Set-Cookie')
    interaction.response.headers.keys.select { |key| key =~ /X-.*/ }.each { |key| interaction.response.headers.delete(key) }
  end
end

def file_fixture(path)
  File.join(Dir.pwd, 'spec', 'fixtures', path)
end
