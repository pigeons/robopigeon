describe RoboPigeon do
  it 'uses the version number from the .version file' do
    expect(RoboPigeon::VERSION).to eq File.read('.version')
  end
end
