require 'spec_helper'

describe RoboPigeon::Dsl::Helpers::GitLab do
  class GitLabHelperTest
    include RoboPigeon::Dsl::Helpers::GitLab
  end
  subject { GitLabHelperTest.new }

  before :each do
    Gitlab.endpoint = 'https://gitlab.example.com/api/v4'
    Gitlab.private_token = 'abc1234'
    RoboPigeon::GitLab::Client.project = 'robopigeon/robopigeon'
  end

  context 'deployment_sha' do
    it 'fetches the sha of a deployment from the gitlab api' do
      VCR.use_cassette('gitlab deployment') do
        expect(subject.deployment_sha('artifactory-gem')).to eq('bd55f29bfb5e779a0d2916607061386af652d332')
      end
    end
  end

  context 'deployment_ref' do
    it 'fetches the sha of a deployment from the gitlab api' do
      VCR.use_cassette('gitlab deployment') do
        expect(subject.deployment_ref('artifactory-gem')).to eq('2.12.0')
      end
    end
  end

  context 'deployment_ref' do
    it 'fetches the sha of a deployment from the gitlab api' do
      VCR.use_cassette('gitlab deployment') do
        expect(subject.deployment_time('artifactory-gem')).to eq('2019-02-04T16:27:20.367Z')
      end
    end
  end
end
