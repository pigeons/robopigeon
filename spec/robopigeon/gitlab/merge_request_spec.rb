require 'spec_helper'

describe RoboPigeon::GitLab::MergeRequest do
  before :each do
    Gitlab.endpoint = 'https://gitlab.com/api/v4'
    Gitlab.private_token = '1234'
    RoboPigeon::GitLab::Client.project = 'robopigeon/robopigeon-test'
    RoboPigeon::GitLab::Client.branch = 'a_branch'
  end

  describe 'create_merge_request' do
    it 'creates a merge request between two branches' do
      VCR.use_cassette('merge request') do
        described_class.create!('a_branch', 'test', 'Automated Merge Request')
      end
    end
    it 'raises an error if target does not exist' do
      VCR.use_cassette('merge request conflict') do
        expect do
          described_class.create!('a_branch', 'test', 'Automated Merge Request')
        end.to raise_error Gitlab::Error::Conflict
      end
    end
  end

  describe 'merge!' do
    it 'creates then merges a merge request' do
      VCR.use_cassette('merge request') do
        described_class.create!('a_branch', 'test', 'Automated Merge Request').merge!
      end
    end
    it 'does nothing if the branch is already up to date' do
      VCR.use_cassette('merge request up to date') do
        described_class.create!('a_branch', 'test', 'Automated Merge Request').merge!
      end
    end
    it 'closes the merge request if the branch does not exist' do
      VCR.use_cassette('merge request no source branch') do
        described_class.create!('no_source_branch', 'master', 'Automated Merge Request').merge!
      end
    end
  end
end
