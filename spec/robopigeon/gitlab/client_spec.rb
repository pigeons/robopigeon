require 'spec_helper'

describe RoboPigeon::GitLab::Client do
  before :each do
    Gitlab.endpoint = 'https://gitlab.com/api/v4'
    Gitlab.private_token = ENV['GITLAB_API_PRIVATE_TOKEN'] || '<GITLAB_API_PRIVATE_TOKEN>'
    RoboPigeon::GitLab::Client.project = 'robopigeon/robopigeon-test'
    RoboPigeon::GitLab::Client.branch = 'a_branch'
  end

  describe 'create_tag' do
    it 'creates a tag' do
      VCR.use_cassette('gitlab create tag') do
        RoboPigeon::GitLab::Client.create_tag('master', '1.0.0', 'Created in a robopigeon test')
      end
    end
    it 'fails if the ref does not exist' do
      VCR.use_cassette('gitlab create tag no branch') do
        expect do
          RoboPigeon::GitLab::Client.create_tag('not_a_branch', '1.0.0', 'Created in a robopigeon test')
        end.to raise_error Gitlab::Error::BadRequest, /Target not_a_branch is invalid/
      end
    end
  end

  describe 'merge_request_comment' do
    it 'posts a message to all merge requests for a branch' do
      VCR.use_cassette('robopigeon-test merge request comments') do
        RoboPigeon::GitLab::Client.merge_request_comment('here is a comment from robopigeon')
      end
    end
    it 'posts a message to a single merge request' do
      VCR.use_cassette('robopigeon-test merge request comments') do
        RoboPigeon::GitLab::Client.merge_request_comment('here is a comment from robopigeon')
      end
    end
    it 'posts a message to a closed merge request' do
      VCR.use_cassette('robopigeon-test includes a closed mr') do
        RoboPigeon::GitLab::Client.merge_request_comment('here is a comment from robopigeon')
      end
    end
    it 'fails if the branch has no merge requests' do
      VCR.use_cassette('robopigeon-test no merge request exists for branch') do
        RoboPigeon::GitLab::Client.branch = 'not_a_branch'
        expect do
          RoboPigeon::GitLab::Client.merge_request_comment('here is a comment from robopigeon')
        end.to raise_error "No merge requests exist for branch 'not_a_branch'"
      end
    end
  end
end
