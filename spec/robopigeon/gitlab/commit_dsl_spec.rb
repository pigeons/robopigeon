require 'spec_helper'

describe RoboPigeon::Dsl::GitLabCommit do
  before :each do
    Gitlab.endpoint = 'https://gitlab.com/api/v4'
    Gitlab.private_token = ENV['GITLAB_API_PRIVATE_TOKEN'] || '<GITLAB_API_PRIVATE_TOKEN>'
    RoboPigeon::GitLab::Client.project = 'pigeons/robopigeon-test'
    RoboPigeon::GitLab::Client.branch = 'a_branch'

    RoboPigeon::Versioner::Version.current = nil
    RoboPigeon::Versioner::Version.files = []
  end

  after :each do
    RoboPigeon::Versioner::Version.current = nil
  end

  it 'has access to helpers' do
    subject.instance_eval do
      git_committer_email
    end
  end

  it 'can add all version files to a commit' do
    expect_any_instance_of(RoboPigeon::GitLab::Commit).to receive(:commit!)
    RoboPigeon::Versioner::Version.files.append(RoboPigeon::Versioner::VersionFile.new(file_fixture('version-good')))
    RoboPigeon::Versioner::Version.files.append(RoboPigeon::Versioner::PackageJson.new(file_fixture('package.json')))
    RoboPigeon::Versioner::Version.current.increment_major

    commit = described_class.run do
      branch 'test'
      version_files
      author 'Pigeon Bot', 'robo.pigeon@example.com'
      message 'Updating a file'
    end
    expect(commit.files.length).to eq(2)
    expect(commit.files.first[:content]).to eq('2.0.0')
  end

  it 'make a commit to a branch' do
    VCR.use_cassette('gitlab commit dsl') do
      described_class.run do
        branch 'test'
        file 'spec/fixtures/stuff.txt', 'updated the content again'
        author 'Pigeon Bot', 'robo.pigeon@example.com'
        message 'Updating a file'
      end
    end
  end
end
