require 'spec_helper'

describe RoboPigeon::Dsl::GitLab do
  before :each do
    Gitlab.endpoint = 'https://gitlab.com/api/v4'
    Gitlab.private_token = '1234'
    RoboPigeon::GitLab::Client.project = 'robopigeon/robopigeon-test'
    RoboPigeon::GitLab::Client.branch = 'a_branch'
  end

  it 'can merge two branches' do
    VCR.use_cassette('merge request') do
      subject.instance_eval do
        merge_branches 'a_branch', 'test'
      end
    end
  end

  it 'has access to helpers' do
    subject.instance_eval do
      git_committer_email
    end
  end

  it 'can create a merge request' do
    VCR.use_cassette('merge request') do
      subject.instance_eval do
        create_merge_request 'a_branch', 'test'
      end
    end
  end

  it 'can look up a merge request' do
    VCR.use_cassette('find merge request') do
      subject.instance_eval do
        find_merge_request 'a_branch', 'test'
      end
    end
  end

  it 'can comment on a merge request for a branch' do
    VCR.use_cassette('comment on merge request') do
      subject.instance_eval do
        merge_request_comment 'test comment'
      end
    end
  end
end
