require 'spec_helper'

describe RoboPigeon::GitLab::Commit do
  before do
    RoboPigeon::GitLab::Client.project = 'pigeons/robopigeon-test'
  end
  it 'adds a file to the list of files' do
    file = file_fixture('stuff.txt')
    subject.add_file file
    expect(subject.files.first).to eq(action: 'update', file_path: file, content: File.read(file))
  end
  it 'adds a file contents when specified' do
    file = file_fixture('stuff.txt')
    subject.add_file file, 'new content'
    expect(subject.files.first).to eq(action: 'update', file_path: file, content: 'new content')
  end
  it 'creates a commit with a single file' do
    subject.message = 'a test commit'
    subject.author_name = 'robopigeon'
    subject.author_email = 'robopigeon@ives.dev'
    subject.branch = 'test'

    subject.add_file 'spec/fixtures/stuff.txt', 'content!', 'create'

    VCR.use_cassette('gitlab commit single file') do
      expect do
        subject.commit!
      end.not_to raise_error
    end
  end
  it 'creates a commit with multiple files' do
    subject.message = 'a multi file test commit'
    subject.author_name = 'robopigeon'
    subject.author_email = 'robopigeon@ives.dev'
    subject.branch = 'test'

    subject.add_file 'more_stuff.txt', 'more content!', 'create'
    subject.add_file 'different_stuff.txt', 'different content!', 'create'

    VCR.use_cassette('gitlab commit multiple files') do
      expect do
        subject.commit!
      end.not_to raise_error
    end
  end
end
