require 'tmpdir'
require 'spec_helper'

describe RoboPigeon::Extensions::Template do
  it 'writes out the default template without erb or othe stuff' do
    dir = Dir.mktmpdir
    RoboPigeon::Extensions::Template.render('test', dir, 'default')
    Dir.glob("#{dir}{**{,/*/**},.*}") do |file|
      expect(file).not_to include('erb')
      expect(file).not_to include('extension')
      unless File.directory?(file)
        expect(File.read(file)).not_to include('<%=')
        expect(File.read(file)).not_to include('%>')
      end
    end
  end

  it 'has tests that pass' do
    cwd = FileUtils.pwd
    dir = Dir.mktmpdir
    RoboPigeon::Extensions::Template.render('test', dir, 'default')
    FileUtils.cd "#{dir}/robopigeon_test"

    expect(system('rspec')).to be true

    FileUtils.cd cwd
  end
end
