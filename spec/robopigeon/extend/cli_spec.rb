require 'spec_helper'
describe RoboPigeon::Extensions::Cli do
  describe '#invoke' do
    it 'requires you to pass a name and defaults to default' do
      expect(RoboPigeon::Extensions::Template).to receive(:render).with('test', FileUtils.pwd, 'default')
      described_class.invoke(%w{new test})
    end

    it 'lets you specifically specify the default tempalte' do
      expect(RoboPigeon::Extensions::Template).to receive(:render).with('test', FileUtils.pwd, 'default')
      described_class.invoke(%w{new --default test})
    end

    it 'lets you specify a path' do
      expect(RoboPigeon::Extensions::Template).to receive(:render).with('test', 'path/to/dir', 'default')
      described_class.invoke(%w{new --path path/to/dir test})
    end
  end
end
