require 'spec_helper'

describe RoboPigeon::Dsl::JenkinsRoot do
  let(:client) { RoboPigeon::Jenkins::Client }
  it 'lets you configure a url' do
    described_class.run do
      api_url 'http://jenkins.example.com'
    end
    expect(client.url).to eq 'http://jenkins.example.com'
  end
  it 'lets you disable the jobs globally' do
    described_class.run do
      enabled false
    end
    expect(client.enabled).to be false
  end
  it 'lets you configure an api user' do
    described_class.run do
      api_user 'pigeon'
    end
    expect(client.user).to eq 'pigeon'
  end
  it 'lets you configure an api token' do
    described_class.run do
      api_key 'pigeonuserkey12345'
    end
    expect(client.token).to eq 'pigeonuserkey12345'
  end
end

describe RoboPigeon::Dsl::Jenkins do
  before do
    RoboPigeon::Jenkins::Client.enabled = true
  end

  it 'has access to helpers' do
    subject.instance_eval do
      git_committer_email
    end
  end

  it 'lets you specify parameters for a job' do
    job = described_class.run false do
      param 'DEPLOY_NOW', 'true'
      param 'ALERT_USER', 'pigeon'
    end
    expect(job.params).to eq('ALERT_USER' => 'pigeon', 'DEPLOY_NOW' => 'true')
  end
  it 'lets you specify a job name' do
    job = described_class.run false do
      name 'robopigeon-test'
    end
    expect(job.name).to eq('robopigeon-test')
  end
  it 'watches a job to completion' do
    VCR.use_cassette 'jenkins watches job until done' do
      job = described_class.run do
        name 'robopigeon-test'
      end
      expect(job.logs).to match(/This is the job output log for robopigeon-test/)
    end
  end
end
