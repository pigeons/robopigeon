require 'spec_helper'

describe RoboPigeon::Jenkins::Job do
  before do
    RoboPigeon::Jenkins::Client.url = ENV['JENKINS_URL'] || 'http://jenkins.example.com/'
    RoboPigeon::Jenkins::Client.user = ENV['JENKINS_USER'] || '<JENKINS_USER>'
    RoboPigeon::Jenkins::Client.token = ENV['JENKINS_TOKEN'] || '<JENKINS_TOKEN>'
  end
  it 'kicks off a build and watches it until it is done' do
    VCR.use_cassette 'jenkins watches job until done' do
      subject.name = 'robopigeon-test'
      subject.poll_interval = 0
      subject.build_and_watch!
      expect(subject.logs).to match(/This is the job output log for robopigeon-test/)
    end
  end
  it 'raises an exception when it times out waiting to start' do
    VCR.use_cassette 'jenkins watches job until start timeout' do
      subject.name = 'robopigeon-test'
      subject.start_timeout = 1
      subject.poll_interval = 0
      expect { subject.build_and_watch! }.to raise_error Timeout::Error
    end
  end
  it 'raises an exception when it times out while running' do
    VCR.use_cassette 'jenkins watches job until completion timeout' do
      subject.name = 'robopigeon-test'
      subject.poll_interval = 1
      subject.build_timeout = 1
      expect { subject.build_and_watch! }.to raise_error Timeout::Error
    end
  end
  it 'raises an exception when it fails' do
    VCR.use_cassette 'jenkins watches job until it fails' do
      subject.name = 'robopigeon-test'
      subject.poll_interval = 1
      subject.build_timeout = 1
      expect { subject.build_and_watch! }.to raise_error RoboPigeon::Jenkins::Failure
    end
  end
end
