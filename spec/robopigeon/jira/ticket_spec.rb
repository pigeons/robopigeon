require 'spec_helper'

describe RoboPigeon::Jira::Ticket do
  before do
    RoboPigeon::Jira::Client.api_key ||= '<JIRA_TOKEN>'
    RoboPigeon::Jira::Client.api_url = 'https://jira.granicus.com'
  end

  after do
    FileUtils.rm('last_created_jira_ticket') if File.exist?('last_created_jira_ticket')
  end

  context 'assign' do
    it 'looks up the user by email and then sets the assignee field' do
      VCR.use_cassette('jira look up assignee') do
        subject.assign('alex.ives@granicus.com')
        expect(subject.assignee).to eq('aives')
      end
    end

    it 'looks up the user by email and then updates the assignee on a created ticket' do
      VCR.use_cassette('jira look up assignee and assign ticket') do
        subject.ticket = 'GOVD-19269'
        subject.assign('alex.ives@granicus.com')
        expect(subject.assignee).to eq('aives')
      end
    end
  end

  context 'set_reporter' do
    it 'looks up the user by email and then sets the reporter field' do
      VCR.use_cassette('jira look up reporter') do
        subject.set_reporter('alex.ives@granicus.com')
        expect(subject.reporter).to eq('aives')
      end
    end
  end

  context 'add_comment' do
    it 'adds a comment to a ticket' do
      VCR.use_cassette('jira add comment to ticket') do
        subject.ticket = 'GOVD-19269'
        subject.add_comment('This is a comment')
      end
    end
  end

  context 'create!' do
    it 'creates the ticket with the provided fields' do
      VCR.use_cassette('jira create ticket') do
        subject.assign('bill.bushey@granicus.com')
        subject.project = 'GOVD'
        subject.set_issuetype 'Spike'
        subject.set_field('Scrum Team', 'Pigeon')
        subject.summary = 'Created a test ticket'
        subject.description = 'This is a test, please ignore.'
        subject.create!
        expect(subject.ticket).not_to be_nil
      end
    end
    it 'throws an exception of a requried field is missing' do
      VCR.use_cassette('jira create ticket with issue') do
        subject.assign('alex.ives@granicus.com')
        subject.project = 'GOVD'
        subject.summary = 'Created a test ticket'
        subject.description = 'This is a test, please ignore.'
        expect { subject.create! }.to raise_error RoboPigeon::Jira::RequiredFieldNotSet
      end
    end
  end

  context 'set_issuetype' do
    it 'looks up the issue type by project' do
      VCR.use_cassette('jira look up issuetypes') do
        subject.project = 'GOVD'
        subject.set_issuetype 'Epic'
      end
    end

    it 'fails if the type was not in the list' do
      VCR.use_cassette('jira look up issuetypes') do
        subject.project = 'GOVD'
        expect { subject.set_issuetype 'Silly Epic' }.to raise_error RoboPigeon::Jira::FieldDoesNotConform
      end
    end

    it 'fails if the project was not set' do
      expect { subject.set_issuetype('Epic') }.to raise_error RoboPigeon::Jira::RequiredFieldNotSet
    end
  end

  context 'attempt_update_from_server' do
    it 'pulls fields from the server and updates them' do
      VCR.use_cassette('jira look up issue') do
        subject.ticket = 'GOVD-19269'
        subject.attempt_update_from_server
      end
    end
    it 'throws an error if there the ticket does not exist' do
      VCR.use_cassette('jira fails to look up issue') do
        subject.ticket = 'TEST-123345'
        expect { subject.attempt_update_from_server }.to raise_error 'Failed to look up issue TEST-123345'
      end
    end
  end

  context 'set_field' do
    it 'looks up the field by name and adds it to the uncreated ticket' do
      VCR.use_cassette('jira look up fields') do
        subject.issue_type = 'Story'
        subject.project = 'GOVD'
        subject.set_field('Scrum Team', 'Pigeon')
        expect(subject.fields).to eq('customfield_11850' => { value: 'Pigeon' })
      end
    end
    it 'looks up the field by name and sets it on a created ticket' do
      VCR.use_cassette('jira set field existing issue') do
        subject.ticket = 'GOVD-19269'
        subject.attempt_update_from_server
        subject.set_field('Scrum Team', 'Pigeon')
      end
    end
    it 'raises an error if there is no field by that name' do
      VCR.use_cassette('jira no field found') do
        subject.issue_type = 'Story'
        subject.project = 'GOVD'
        expect { subject.set_field('Hum Drum Team', 'Pigeon') }.to raise_error 'Field Hum Drum Team was not found'
      end
    end
    it 'transforms a value as needed for a string array field' do
      VCR.use_cassette('jira look up fields') do
        subject.issue_type = 'Story'
        subject.project = 'GOVD'
        subject.set_field('Labels', 'First Label, Second Label,Third Label ,Fourth Label')
        expect(subject.fields).to eq('labels' => ['First Label', 'Second Label', 'Third Label', 'Fourth Label'])
      end
    end
    it 'transforms a value as needed for a multiselect array field' do
      VCR.use_cassette('jira look up fields') do
        subject.issue_type = 'Story'
        subject.project = 'GOVD'
        subject.set_field('Granicus Projects / Products', 'First Project, Second Project,Third Project,Fourth Project')
        expect(subject.fields)
          .to eq('customfield_14958' => [
                   { value: 'First Project' },
                   { value: 'Second Project' },
                   { value: 'Third Project' },
                   { value: 'Fourth Project' }
                 ])
      end
    end
  end

  context 'perform_transition' do
    it 'looks up the transition fields and performs the transition' do
      VCR.use_cassette('jira look up and transition issue') do
        subject.ticket = 'GOVD-19269'
        subject.perform_transition('Groomed')
      end
    end
    it 'fails if the ticket was not already created' do
      expect { subject.current_state }.to raise_error RoboPigeon::Jira::TicketNotFoundOrSet
    end
  end

  context 'current_state' do
    it 'looks up the current transition state and returns it' do
      VCR.use_cassette('jira look up current state') do
        subject.ticket = 'GOVD-19269'
        expect(subject.current_state).to eq('Ready')
      end
    end
    it 'fails if the ticket was not already created' do
      expect { subject.current_state }.to raise_error RoboPigeon::Jira::TicketNotFoundOrSet
    end
  end

  context 'wait_for_state!' do
    it 'returns immediately if the transition has already happened' do
      subject.ticket = 'GOVD-19269'
      expect(subject).to receive(:current_state).and_return('Ready')
      subject.wait_for_state!('Ready')
    end
    it 'returns after a checking three times' do
      subject.ticket = 'GOVD-19269'
      expect(subject).to receive(:current_state).and_return('Not Ready', 'Not Ready', 'Ready')
      subject.wait_for_state!('Ready', 10.minutes, Time.now, 0)
    end
    it 'times out if the transition is not found in the specified time period' do
      subject.ticket = 'GOVD-19269'
      expect(subject).to receive(:current_state).and_return('Not Ready', 'Not Ready', 'Not Ready')
      expect { subject.wait_for_state!('Ready', 2.seconds, Time.now, 1) }.to raise_error RoboPigeon::Jira::WaitTimeout
    end
    it 'fails if the ticket was not already created' do
      expect { subject.current_state }.to raise_error RoboPigeon::Jira::TicketNotFoundOrSet
    end
  end
end
