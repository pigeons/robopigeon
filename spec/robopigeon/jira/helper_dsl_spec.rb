require 'spec_helper'

describe RoboPigeon::Dsl::Helpers::Jira do
  class JiraHelperTest
    include RoboPigeon::Dsl::Helpers
  end
  subject { JiraHelperTest.new }

  before do
    RoboPigeon::Jira::Client.last_created_ticket = 'PIGEON-1234'
    RoboPigeon::Jira::Client.api_url = 'https://jira.example.com'
  end

  context 'jira_last_created_ticket' do
    it 'returns the last created ticket' do
      expect(subject.jira_last_created_ticket).to eq('PIGEON-1234')
    end
  end

  context 'jira_last_created_ticket_link' do
    it 'returns a link to the last created ticket' do
      expect(subject.jira_last_created_ticket_link).to eq('https://jira.example.com/browse/PIGEON-1234')
    end
  end

  context 'jira_last_created_ticket_slack_link' do
    it 'returns a slack link to the last created ticket' do
      expect(subject.jira_last_created_ticket_slack_link).to eq('<https://jira.example.com/browse/PIGEON-1234|PIGEON-1234>')
    end
  end
end
