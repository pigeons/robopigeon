require 'spec_helper'

describe RoboPigeon::Dsl::JiraTicket do
  before :each do
    RoboPigeon::Jira::Client.api_url = 'https://jira.granicus.com'
    RoboPigeon::Jira::Client.api_key ||= '<JIRA_TOKEN>'
  end

  after do
    FileUtils.rm('last_created_jira_ticket') if File.exist?('last_created_jira_ticket')
  end

  it 'has access to helpers' do
    subject.instance_eval do
      git_committer_email
    end
  end

  context 'new ticket, skip create' do
    before do
      allow_any_instance_of(RoboPigeon::Jira::Ticket).to receive(:create!)
    end

    it 'adds the correct assignee' do
      VCR.use_cassette('jira look up assignee') do
        ticket = described_class.run do
          assign 'alex.ives@granicus.com'
        end
        expect(ticket.assignee).to eq('aives')
      end
    end

    it 'adds the project' do
      ticket = described_class.run do
        project 'PIG'
      end
      expect(ticket.project).to eq('PIG')
    end

    it 'sets the summary' do
      ticket = described_class.run do
        summary 'This is a summary'
      end
      expect(ticket.summary).to eq('This is a summary')
    end

    it 'sets the issue type' do
      VCR.use_cassette('jira look up issuetypes') do
        ticket = described_class.run do
          project 'GOVD'
          issuetype 'Story'
        end
        expect(ticket.issue_type).to eq('Story')
      end
    end

    it 'sets the description' do
      ticket = described_class.run do
        description 'This is a description'
      end
      expect(ticket.description).to eq('This is a description')
    end

    it 'sets a field' do
      VCR.use_cassette('jira dsl look up fields') do
        ticket = described_class.run do
          project 'GOVD'
          issuetype 'Story'
          field 'Scrum Team', 'Pigeon'
        end
        expect(ticket.fields).to eq('customfield_11850' => { value: 'Pigeon' })
      end
    end
  end

  context 'creates new tickets' do
    it 'creates the ticket on command' do
      expected_ticket = 'GOVD-17844'
      VCR.use_cassette('jira dsl create ticket') do
        ticket = described_class.run do
          project 'GOVD'
          issuetype 'Story'
          field 'Scrum Team', 'Pigeon'
          summary 'test issue'
          description 'this issue is a test, please ignore'
          create!
          raise "Expected #{@ticket.ticket} to equal '#{expected_ticket}'" unless @ticket.ticket == expected_ticket
        end
        expect(ticket.ticket).to eq(expected_ticket)
      end
    end

    it 'creates the ticket if given enough information' do
      VCR.use_cassette('jira dsl create ticket') do
        ticket = described_class.run do
          project 'GOVD'
          issuetype 'Story'
          field 'Scrum Team', 'Pigeon'
          summary 'test issue'
          description 'this issue is a test, please ignore'
        end
        expect(ticket.ticket).to eq('GOVD-17844')
      end
    end
  end

  context 'existing ticket' do
    it 'runs comments' do
      VCR.use_cassette('jira add comment to ticket') do
        described_class.run 'GOVD-19269' do
          comment('This is a comment')
        end
      end
    end

    it 'runs assigns' do
      VCR.use_cassette('jira look up assignee and assign ticket') do
        ticket = described_class.run 'GOVD-19269' do
          assign 'alex.ives@granicus.com'
        end
        expect(ticket.assignee).to eq('aives')
      end
    end

    it 'runs field' do
      VCR.use_cassette('jira set field existing issue') do
        ticket = described_class.run 'GOVD-19269' do
          field 'Scrum Team', 'Pigeon'
        end
        expect(ticket.fields).to eq('customfield_11850' => { value: 'Pigeon' })
      end
    end

    it 'runs print_id' do
      VCR.use_cassette('jira set field existing issue') do
        expect_any_instance_of(RoboPigeon::Dsl::JiraTicket).to receive(:puts).with('GOVD-19269')
        described_class.run 'GOVD-19269' do
          print_id
        end
      end
    end

    it 'runs transition' do
      VCR.use_cassette('jira look up and transition issue') do
        described_class.run 'GOVD-19269' do
          transition 'Groomed'
        end
      end
    end

    it 'runs wait_for_state' do
      VCR.use_cassette('jira look up and transition issue') do
        expect_any_instance_of(RoboPigeon::Jira::Ticket).to receive(:current_state).and_return('Ready')
        described_class.run 'GOVD-19269' do
          wait_for_state 'Ready'
        end
      end
    end
  end
end
