require 'spec_helper'

describe RoboPigeon::Dsl::Jira do
  before :each do
    RoboPigeon::Jira::Client.api_url = 'https://jira.granicus.com'
    RoboPigeon::Jira::Client.api_key ||= '<JIRA_TOKEN>'
  end

  it 'comments on a ticket' do
    VCR.use_cassette('jira add comment to ticket') do
      subject.instance_eval do
        comment_on 'GOVD-19269', 'This is a comment'
      end
    end
  end

  it 'has access to helpers' do
    subject.instance_eval do
      git_committer_email
    end
  end

  it 'calls run on the ticket dsl' do
    expect(RoboPigeon::Dsl::JiraTicket).to receive(:run).with('PIGEON-1234')
    subject.instance_eval do
      ticket 'PIGEON-1234'
    end
  end
end
