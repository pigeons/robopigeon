describe RoboPigeon::Dsl::Job do
  let(:test_message) { RoboPigeon::Slack::Message.new }

  before do
    RoboPigeon::Slack::Client.enabled = true
  end

  it 'has a slack method' do
    allow_any_instance_of(RoboPigeon::Slack::Message).to receive(:send!)
    subject.instance_eval do
      slack do
        raise "Expected class to be RoboPigeon::Dsl::Slack but was #{self.class}" unless self.class == RoboPigeon::Dsl::Slack
      end
    end
  end

  it 'looks up and adds the helper if it shows up after method missing' do
    subject.instance_eval do
      slack do
        git_committer_email
        raise "Expected to see helpers but didn't" unless methods.include?(:git_committer_email)
      end
    end
  end

  it 'has access to helpers' do
    expect do
      subject.instance_eval do
        slack do
          git_committer_email_not_really_though_this_does_not_exist
        end
      end
    end.to raise_error(NameError)
  end
end
