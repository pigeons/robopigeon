require 'spec_helper'

describe RoboPigeon::Dsl::Root do
  it 'initializes puts a job in the jobs hash' do
    subject = described_class.new(file_fixture('configs/empty_job.rb'))
    expect_any_instance_of(RoboPigeon::Dsl::Job).to receive(:puts).with('hello')
    subject.run 'hello'
  end

  it 'calls into slack at the top level' do
    described_class.new(file_fixture('configs/slack_top_level_config.rb'))
    expect(RoboPigeon::Slack::Client.api_key).to eq('abc1234')
    expect(RoboPigeon::Slack::Client.name).to eq('Automated Pigeon Bot')
    expect(RoboPigeon::Slack::Client.emoji).to eq(':pigeon-bot:')
  end

  it 'calls into gitlab at the top level' do
    described_class.new(file_fixture('configs/gitlab_top_level.rb'))
    expect(Gitlab.endpoint).to eq('https://gitlab.example.com/api/v4')
    expect(Gitlab.private_token).to eq('abc1234')
  end

  it 'always has a help task' do
    subject = described_class.new(file_fixture('configs/empty_job.rb'))
    expect(subject.jobs).to include('help')
    expect(subject.jobs).to include('-h')
  end

  it 'always has a version task' do
    subject = described_class.new(file_fixture('configs/empty_job.rb'))
    expect(subject.jobs).to include('version')
    expect(subject.jobs).to include('-v')
  end
end
