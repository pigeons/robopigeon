require 'spec_helper'

describe RoboPigeon::Versioner::Pom do
  let(:version) { RoboPigeon::Versioner::Semver.new('2.3.4') }
  before do
    allow(RoboPigeon::Versioner::Version).to receive(:current).and_return(version)
  end

  it 'should raise an error if the file does not exist' do
    expect do
      described_class.new('does-not-exist')
    end.to raise_error('You must create version file does-not-exist to continue')
  end

  it 'should raise an error if the file contents are bad' do
    expect do
      described_class.new(file_fixture('pom/bad.xml')).read
    end.to raise_error("Version file #{file_fixture('pom/bad.xml')} must be in the format /[0-9]+\\.[0-9]+\\.[0-9]+/ but was Cheese")
  end

  context 'no snapshot' do
    subject { described_class.new(file_fixture('pom/normal.xml'), '-SNAPSHOT') }

    it 'loads a version from a pom file' do
      expect(subject.read.to_s).to eq('1.2.3')
    end

    it 'updates a non-snapshot version' do
      expect(subject.updated(false)).to include('2.3.4')
    end

    it 'adds a snapshot to the version number if asked' do
      expect(subject.updated(true)).to include('2.3.4-SNAPSHOT')
    end
  end

  context 'snapshot' do
    subject { described_class.new(file_fixture('pom/snapshot.xml'), '-SNAPSHOT') }

    it 'loads a version from a pom file with a snapshot' do
      expect(subject.read.to_s).to eq('1.2.3')
    end

    it 'updates and removes snapshot' do
      updated_content = subject.updated(false)
      expect(updated_content).to include('2.3.4')
      expect(updated_content).not_to include('-SNAPSHOT')
    end

    it 'updates and includes snapshot' do
      expect(subject.updated(true)).to include('2.3.4-SNAPSHOT')
    end
  end

  context '#write' do
    let(:file) { file_fixture('pom/normal.xml.test') }

    before :each do
      FileUtils.cp(file_fixture('pom/normal.xml'), file)
    end
    after :each do
      FileUtils.rm(file)
    end

    subject { described_class.new(file) }

    it 'writes a version' do
      subject.write
      expect(File.read(file)).to include('2.3.4')
    end
  end
end
