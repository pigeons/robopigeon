require 'spec_helper'

describe RoboPigeon::Dsl::Helpers::Versioner do
  class VersionerHelperTest
    include RoboPigeon::Dsl::Helpers::Versioner
  end
  let(:version_file) { RoboPigeon::Versioner::VersionFile.new(file_fixture('version-good')) }
  subject { VersionerHelperTest.new }

  context 'version' do
    it 'returns the current version from the version file' do
      RoboPigeon::Versioner::Version.current = nil
      RoboPigeon::Versioner::Version.files = [version_file]
      expect(subject.version).to eq('1.2.3')
    end
  end
end
