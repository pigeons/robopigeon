require 'spec_helper'

describe RoboPigeon::Versioner::Semver do
  subject { described_class.new('1.2.3') }

  context '.version file' do
    it 'should parse the version' do
      expect(subject.to_s).to eq('1.2.3')
      expect(subject.major).to eq(1)
      expect(subject.minor).to eq(2)
      expect(subject.patch).to eq(3)
    end

    it 'should be able to increment major versions' do
      expect(subject.major).to eq(1)
      expect(subject.increment_major).to eq('2.0.0')
      expect(subject.major).to eq(2)
      expect(subject.minor).to eq(0)
      expect(subject.patch).to eq(0)
    end

    it 'should be able to increment minor versions' do
      expect(subject.minor).to eq(2)
      expect(subject.increment_minor).to eq('1.3.0')
      expect(subject.minor).to eq(3)
      expect(subject.patch).to eq(0)
    end

    it 'should be able to increment patch versions' do
      expect(subject.patch).to eq(3)
      expect(subject.increment_patch).to eq('1.2.4')
      expect(subject.patch).to eq(4)
    end
  end

  context 'equality' do
    it 'should match an array of three integers for [major, minor patch]' do
      expect(described_class.new('1.2.3')).to eq([1, 2, 3])
    end

    it 'should match a hash with keys :major, :minor, :patch' do
      expect(described_class.new('1.2.3')).to eq(major: 1, minor: 2, patch: 3)
    end

    it 'should match another Semver object' do
      expect(described_class.new('1.2.3')).to eq(described_class.new([1, 2, 3]))
    end

    it 'should not match an incorrect array' do
      expect(described_class.new('1.2.3')).not_to eq([1, 2, 4])
    end

    it 'should not match an incorrect hash' do
      expect(described_class.new('1.2.3')).not_to eq(major: 1, minor: 2, patch: 4)
      expect(described_class.new('1.2.3')).not_to eq(major: 1, minor: 3, patch: 3)
      expect(described_class.new('1.2.3')).not_to eq(major: 2, minor: 2, patch: 3)
    end

    it 'should not match an incorrect object' do
      expect(described_class.new('1.2.3')).not_to eq(described_class.new([1, 2, 4]))
    end

    it 'should not match some random thing' do
      expect(described_class.new('1.2.3')).not_to eq(described_class.new(Set.new))
    end

    it 'should match some the string of the version' do
      expect(described_class.new('1.2.3')).to eq('1.2.3')
    end

    it 'should not match an incorrect string of the version' do
      expect(described_class.new('1.2.3')).not_to eq('1.2.4')
    end
  end
end
