require 'spec_helper'

describe RoboPigeon::Versioner::VersionFile do
  let(:version) { RoboPigeon::Versioner::Semver.new('2.3.4') }
  before do
    allow(RoboPigeon::Versioner::Version).to receive(:current).and_return(version)
  end

  it 'should raise an error if the file does not exist' do
    expect do
      described_class.new('does-not-exist')
    end.to raise_error('You must create version file does-not-exist to continue')
  end

  context 'initialized block' do
    it 'defaults to just calling into the current version' do
      expect(RoboPigeon::Versioner::Version).to receive(:current).and_return(version)
      file = described_class.new(file_fixture('version-good'))
      expect(file.version_lambda.call).to eq(version.to_s)
    end

    it 'accepts a block' do
      expect(RoboPigeon::Versioner::Version).to receive(:current).and_return(version)
      file = described_class.new(file_fixture('version-good')) do |current|
        "boo #{current}"
      end
      expect(file.version_lambda.call).to eq("boo #{version}")
    end
  end

  context 'good file' do
    subject { described_class.new(file_fixture('version-good')) { '2.3.4' } }

    it 'should read the provided file' do
      expect(subject.read.to_s). to eq('1.2.3')
    end

    it 'gets an updated version' do
      expect(subject.updated).to eq('2.3.4')
    end
  end

  context 'bad file' do
    subject { described_class.new(file_fixture('version-bad')) }
    it 'should raise an error if the file contents are bad' do
      expect do
        subject.read
      end.to raise_error("Version file #{file_fixture('version-bad')} must be in the format /[0-9]+\\.[0-9]+\\.[0-9]+/ but was a.b.c")
    end
  end

  context '#write' do
    let(:file) { file_fixture('version-good.test') }

    before :each do
      FileUtils.cp(file_fixture('version-good'), file)
    end
    after :each do
      FileUtils.rm(file)
    end

    subject { described_class.new(file) }

    it 'writes a version' do
      subject.write
      expect(File.read(file)).to eq('2.3.4')
    end
  end

  context '#from_type' do
    it 'makes a normal file class' do
      expect(described_class.from_type(file_fixture('version-good'))).to be_a(described_class)
    end

    it 'makes a normal file class while taking a block' do
      result = described_class.from_type(file_fixture('version-good')) do |current|
        "#{current} wow"
      end
      expect(result).to be_a(described_class)
    end

    it 'makes a regex file class' do
      expect(described_class.from_type(file_fixture('regex_version_files/Version.java'), type: :regex, regex: /^    public static String VERSION = '(\d+\.\d+\.\d+)';$/)).to be_a(RoboPigeon::Versioner::Regex)
    end

    it 'makes a regex file class while taking a block' do
      result = described_class.from_type(file_fixture('regex_version_files/Version.java'), type: :regex, regex: /^    public static String VERSION = '(\d+\.\d+\.\d+)';$/) do |current|
        "#{current} wow"
      end
      expect(result).to be_a(RoboPigeon::Versioner::Regex)
    end

    it 'makes a normal file class' do
      expect(described_class.from_type(file_fixture('pom/normal.xml'), type: :pom)).to be_a(RoboPigeon::Versioner::Pom)
    end

    it 'makes a normal file class while taking a block' do
      result = described_class.from_type(file_fixture('pom/normal.xml'), type: :pom) do |current|
        "#{current} wow"
      end
      expect(result).to be_a(RoboPigeon::Versioner::Pom)
    end

    it 'makes a package.json class' do
      expect(described_class.from_type(file_fixture('package.json'), type: :package_json)).to be_a(RoboPigeon::Versioner::PackageJson)
    end

    it 'makes a package.json class while taking a block' do
      result = described_class.from_type(file_fixture('package.json'), type: :package_json) do |current|
        "#{current} wow"
      end
      expect(result).to be_a(RoboPigeon::Versioner::PackageJson)
    end

    it 'makes an android class' do
      result = described_class.from_type(file_fixture('regex_version_files/android.gradle'), type: :android_gradle) do |m, i, p|
        m * 100 + i * 10 + p
      end
      expect(result).to be_a(RoboPigeon::Versioner::AndroidGradle)
    end
  end
end
