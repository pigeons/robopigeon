describe RoboPigeon::Dsl::VersionerRoot do
  before :each do
    RoboPigeon::Versioner::Version.current = nil
    RoboPigeon::Versioner::Version.files = []
  end

  context 'plain files' do
    it 'adds a file' do
      expect(RoboPigeon::Versioner::Version.current).to be_nil
      subject.instance_eval do
        file file_fixture('version-good')
      end
      expect(RoboPigeon::Versioner::Version.current.to_s).to eq('1.2.3')
      RoboPigeon::Versioner::Version.current.increment_major
      expect(RoboPigeon::Versioner::Version.files.first.updated).to include('2.0.0')
    end
  end

  context 'pom files' do
    it 'adds a file' do
      expect(RoboPigeon::Versioner::Version.current).to be_nil
      subject.instance_eval do
        pom_file file_fixture('pom/normal.xml')
      end
      expect(RoboPigeon::Versioner::Version.current.to_s).to eq('1.2.3')
      RoboPigeon::Versioner::Version.current.increment_major
      expect(RoboPigeon::Versioner::Version.files.first.updated).to include('2.0.0')
    end

    it 'correctly processes a block' do
      expect(RoboPigeon::Versioner::Version.current).to be_nil
      subject.instance_eval do
        pom_file file_fixture('pom/normal.xml') do |current|
          "wooo #{current}"
        end
      end
      expect(RoboPigeon::Versioner::Version.current.to_s).to eq('1.2.3')
      RoboPigeon::Versioner::Version.current.increment_major
      expect(RoboPigeon::Versioner::Version.files.first.updated).to include('wooo 2.0.0')
    end

    it 'adds a file with a snapshot in it' do
      expect(RoboPigeon::Versioner::Version.current).to be_nil
      subject.instance_eval do
        pom_file file_fixture('pom/snapshot.xml')
      end
      expect(RoboPigeon::Versioner::Version.current.to_s).to eq('1.2.3')
      RoboPigeon::Versioner::Version.current.increment_major
      expect(RoboPigeon::Versioner::Version.files.first.updated).to include('2.0.0')
    end
  end

  context 'package_json_file' do
    it 'adds a file' do
      expect(RoboPigeon::Versioner::Version.current).to be_nil
      subject.instance_eval do
        package_json_file file_fixture('package.json')
      end
      expect(RoboPigeon::Versioner::Version.current.to_s).to eq('1.2.3')
      RoboPigeon::Versioner::Version.current.increment_major
      expect(RoboPigeon::Versioner::Version.files.first.updated).to include('2.0.0')
    end

    it 'correctly processes a block' do
      expect(RoboPigeon::Versioner::Version.current).to be_nil
      subject.instance_eval do
        package_json_file file_fixture('package.json') do |current|
          "wooo #{current}"
        end
      end
      expect(RoboPigeon::Versioner::Version.current.to_s).to eq('1.2.3')
      RoboPigeon::Versioner::Version.current.increment_major
      expect(RoboPigeon::Versioner::Version.files.first.updated).to include('wooo 2.0.0')
    end
  end
  context 'regex file' do
    it 'adds a file' do
      expect(RoboPigeon::Versioner::Version.current).to be_nil
      subject.instance_eval do
        regex_file file_fixture('regex_version_files/Version.java'), /^    public static String VERSION = '(\d+\.\d+\.\d+)';$/
      end
      expect(RoboPigeon::Versioner::Version.current.to_s).to eq('1.2.3')
      RoboPigeon::Versioner::Version.current.increment_major
      expect(RoboPigeon::Versioner::Version.files.first.updated).to include('2.0.0')
    end

    it 'correctly processes a block' do
      expect(RoboPigeon::Versioner::Version.current).to be_nil
      subject.instance_eval do
        regex_file file_fixture('regex_version_files/Version.java'), /^    public static String VERSION = '(\d+\.\d+\.\d+)';$/ do |current|
          "wooo #{current}"
        end
      end
      expect(RoboPigeon::Versioner::Version.current.to_s).to eq('1.2.3')
      RoboPigeon::Versioner::Version.current.increment_major
      expect(RoboPigeon::Versioner::Version.files.first.updated).to include('wooo 2.0.0')
    end
  end
  context 'android_gradle_file' do
    it 'adds a file' do
      expect(RoboPigeon::Versioner::Version.current).to be_nil
      subject.instance_eval do
        android_gradle_file file_fixture('regex_version_files/android.gradle') do |major, minor, patch|
          "#{major}00#{minor}00#{patch}" # Don't actually do this, that would be dumb.
        end
      end
      expect(RoboPigeon::Versioner::Version.current.to_s).to eq('1.2.3')
      RoboPigeon::Versioner::Version.current.increment_major
      expect(RoboPigeon::Versioner::Version.files.first.updated).to include('2.0.0')
      expect(RoboPigeon::Versioner::Version.files.first.updated).to include('2000000')
    end
    it 'raises an error if not given a block' do
      expect do
        subject.instance_eval do
          android_gradle_file file_fixture('regex_version_files/android.gradle')
        end
      end.to raise_error('The android versionCode requires a block')
    end
  end
end

describe RoboPigeon::Dsl::Versioner do
  let(:version) { RoboPigeon::Versioner::Semver.new('1.2.3') }
  before do
    allow(RoboPigeon::Versioner::Version).to receive(:current).and_return(version)
  end
  after :each do
    RoboPigeon::Versioner::Version.current = nil
    RoboPigeon::Versioner::Version.files = []
  end
  it 'increments the major version in current' do
    subject.instance_eval do
      increment_major
    end
    expect(RoboPigeon::Versioner::Version.current).to eq([2, 0, 0])
  end
  it 'increments the minor version in current' do
    subject.instance_eval do
      increment_minor
    end
    expect(RoboPigeon::Versioner::Version.current).to eq([1, 3, 0])
  end
  it 'increments the patch version in current' do
    subject.instance_eval do
      increment_patch
    end
    expect(RoboPigeon::Versioner::Version.current).to eq([1, 2, 4])
  end
end
