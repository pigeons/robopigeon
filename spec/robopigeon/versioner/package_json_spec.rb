require 'spec_helper'

describe RoboPigeon::Versioner::PackageJson do
  let(:version) { RoboPigeon::Versioner::Semver.new('2.3.4') }
  before do
    allow(RoboPigeon::Versioner::Version).to receive(:current).and_return(version)
  end

  subject { described_class.new(file_fixture('package.json')) }
  it 'should read the provided file' do
    expect(subject.read.to_s).to eq('1.2.3')
  end

  it 'should raise an error if the file does not exist' do
    expect do
      described_class.new('does-not-exist')
    end.to raise_error('You must create version file does-not-exist to continue')
  end

  it 'should update a file' do
    expect(subject.updated).to include(version.to_s)
    JSON.parse(subject.updated)
  end
end
