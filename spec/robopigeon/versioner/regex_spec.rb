require 'spec_helper'

describe RoboPigeon::Versioner::Regex do
  let(:version) { RoboPigeon::Versioner::Semver.new('2.3.4') }
  before do
    allow(RoboPigeon::Versioner::Version).to receive(:current).and_return(version)
  end

  it 'should raise an error if the file does not exist' do
    expect do
      described_class.new('does-not-exist')
    end.to raise_error('You must create version file does-not-exist to continue')
  end

  context 'java file' do
    let(:regex) { /^    public static String VERSION = '(\d+\.\d+\.\d+)';$/ }

    subject { described_class.new(file_fixture('regex_version_files/Version.java'), regex) }

    it 'should be able to read a version from it' do
      expect(subject.read.to_s).to eq('1.2.3')
    end

    it 'should be able to write out a regex with an updated version' do
      expect(subject.updated).to include('2.3.4')
    end
  end

  context 'plain file' do
    subject { described_class.new(file_fixture('regex_version_files/plain.txt')) }

    it 'should be able to read a version from it' do
      expect(subject.read.to_s).to eq('1.2.3')
    end

    it 'should be able to write out a regex with an updated version' do
      expect(subject.updated).to include('2.3.4')
    end
  end

  context 'write android gradle versionCode' do
    subject do
      described_class.new(file_fixture('regex_version_files/android.gradle'), /^    versionCode (\d+)$/) do |current|
        current.major * 1_000_000 + current.minor * 1_000 + current.patch
      end
    end

    it 'should be able to write out a regex with a formatted version' do
      expect(subject.updated).to include('versionCode 2003004')
    end
  end

  context 'android gradle versionName' do
    subject { described_class.new(file_fixture('regex_version_files/android.gradle'), /^    versionName \"(\d+\.\d+\.\d+)\"$/) }

    it 'should be able to read a version from it' do
      expect(subject.read.to_s).to eq('1.2.3')
    end

    it 'should be able to write out a regex with a formatted version' do
      expect(subject.updated).to include('versionName "2.3.4"')
    end
  end
end
