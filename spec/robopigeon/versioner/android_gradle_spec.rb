require 'spec_helper'

describe RoboPigeon::Versioner::AndroidGradle do
  let(:version) { RoboPigeon::Versioner::Semver.new('2.3.4') }
  before do
    allow(RoboPigeon::Versioner::Version).to receive(:current).and_return(version)
  end

  it 'should raise an error if the file does not exist' do
    expect do
      described_class.new('does-not-exist')
    end.to raise_error('You must create version file does-not-exist to continue')
  end

  subject do
    described_class.new(file_fixture('regex_version_files/android.gradle')) do |major, minor, patch|
      major * 1_000_000 + minor * 1_000 + patch
    end
  end

  it 'should be able to update out a regex with a formatted version' do
    updated_contents = subject.updated
    expect(updated_contents).to include('versionCode 2003004')
    expect(updated_contents).to include('versionName "2.3.4"')
  end

  it 'should be able to read a version from it' do
    expect(subject.read.to_s).to eq('1.2.3')
  end
end
