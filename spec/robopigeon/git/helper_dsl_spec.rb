require 'spec_helper'
require 'fileutils'

describe RoboPigeon::Dsl::Helpers::Git do
  class GitHelperTest
    include RoboPigeon::Dsl::Helpers::Git
  end

  def within_git_repo(repo_fixture_name)
    cwd = FileUtils.pwd
    repo_path = file_fixture("git/#{repo_fixture_name}/")
    FileUtils.rm_rf(repo_path) if File.exist?(repo_path)
    `tar xf #{file_fixture("git/#{repo_fixture_name}.tar")} -C #{file_fixture('git')}`
    FileUtils.cd repo_path
    yield
    FileUtils.cd cwd
    FileUtils.rm_rf repo_path
  end

  subject { GitHelperTest.new }

  shared_examples_for 'git methods' do |method, no_merges_result, normal_merges_result, upstream_merge_result|
    it "returns #{no_merges_result.empty? ? 'empty string' : no_merges_result} for #{method} when no merges exist" do
      within_git_repo 'no_merges' do
        expect(subject.send(method)).to eq(no_merges_result)
      end
    end
    it "returns #{normal_merges_result} for #{method} when normal merges exist" do
      within_git_repo 'normal_merge_commit' do
        expect(subject.send(method)).to eq(normal_merges_result)
      end
    end
    it "returns #{upstream_merge_result} for #{method} when upstream merges exist" do
      within_git_repo 'merge_branch_into_upstream' do
        expect(subject.send(method)).to eq(upstream_merge_result)
      end
    end
  end

  describe 'git_committer_name' do
    it_behaves_like 'git methods', :git_committer_name, 'Alex Ives', 'Alex Ives', 'Alex Ives'
  end

  describe 'git_committer_email' do
    it_behaves_like 'git methods', :git_committer_email, 'alex.ives@granicus.com', 'alex.ives@granicus.com', 'alex.ives@granicus.com'
  end

  describe 'git_merger_name' do
    it_behaves_like 'git methods', :git_merger_name, '', 'Alex Ives', 'Alex Ives'
  end

  describe 'git_merger_email' do
    it_behaves_like 'git methods', :git_merger_email, '', 'alex.ives@granicus.com', 'alex.ives@granicus.com'
  end

  describe 'git_branch_merged_source' do
    it_behaves_like 'git methods', :git_branch_merged_source, 'unknown', 'another_branch', 'origin/master'
  end

  describe 'git_branch_merged_target' do
    it_behaves_like 'git methods', :git_branch_merged_target, 'unknown', 'master', 'master'
  end
end
