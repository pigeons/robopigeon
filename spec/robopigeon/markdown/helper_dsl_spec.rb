require 'spec_helper'

describe RoboPigeon::Dsl::Helpers::Markdown do
  class MarkdownHelperTest
    include RoboPigeon::Dsl::Helpers
  end
  subject { MarkdownHelperTest.new }

  let(:basic_file) { file_fixture('markdown/basic.erb.md') }
  let(:dsl_file) { file_fixture('markdown/dsl.erb.md') }
  let(:link_file) { file_fixture('markdown/link.erb.md') }

  it 'converts a markdown file to confluence/jira' do
    expect(subject.jira_from_md(basic_file)).to eq("h3. Test some stuff!\n[Link|https://gitlab.com/pigeons/robopigeon]\n{code}Stuff!\n{code}\n")
    expect(subject.confluence_from_md(basic_file)).to eq(subject.jira_from_md(basic_file))
  end
  it 'lets you use functions from the other helpers' do
    expect(subject).to receive(:git_committer_name).and_return('Alex Ives')
    expect(subject.html_from_md(dsl_file)).to eq("<h2 id=\"test-some-stuff\">Test some stuff!</h2>\n\n<p>Alex Ives</p>\n")
  end
  it 'converts a markdown file to html' do
    expect(subject.html_from_md(basic_file)).to eq("<h3 id=\"test-some-stuff\">Test some stuff!</h3>\n<p><a href=\"https://gitlab.com/pigeons/robopigeon\">Link</a></p>\n<pre><code>Stuff!\n</code></pre>\n")
  end
  it 'throws an error if the file does not exist' do
    expect { subject.html_from_md('does_not_exist.erb.md') }.to raise_error(RoboPigeon::Markdown::Error, 'template file does_not_exist.erb.md not found')
  end
  it 'converts a markdown file to a slack message' do
    expect(subject.slack_from_md(basic_file)).to eq("\n\n*Test some stuff!*\nhttps://gitlab.com/pigeons/robopigeon\n\n```\nStuff!\n```\n\n")
  end
end
