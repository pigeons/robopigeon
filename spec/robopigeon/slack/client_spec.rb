describe RoboPigeon::Slack::Client do
  subject do
    described_class.api_key = ENV['SLACK_API_KEY']
    described_class
  end
  it 'raises an error if the api key was not set and client is referenced' do
    subject.client = nil
    expect { subject.client }.to raise_error('client requires API key be set')
  end
  it 'raises an error if the api key was not set and the api key is requestd' do
    subject.client = Slack::Web::Client.new(token: nil)
    expect { subject.api_key }.to raise_error('api_key requires API key be set')
  end
  context 'get_user' do
    it 'returns nil if a user is not found' do
      VCR.use_cassette('user not found') do
        expect(subject.get_user(['There is nobody with this name'])).to be_nil
      end
    end
    it 'returns a user id for a user' do
      VCR.use_cassette('user was found') do
        expect(subject.get_user(['A person'])[:id]).to eq('U0L0MRPB8')
      end
    end
  end
end
