require 'spec_helper'

describe RoboPigeon::Dsl::Helpers::Slack do
  class SlackHelperTest
    include RoboPigeon::Dsl::Helpers::Slack
  end
  subject { SlackHelperTest.new }

  context 'slack_user_for' do
    it 'calls into the slack client to look up the user and returns a formatted link' do
      expect(RoboPigeon::Slack::Client).to receive(:get_user).with(['test']).and_return(OpenStruct.new(id: '12345'))
      expect(subject.slack_user_for('test')).to eq('<@12345>')
    end

    it 'returns empty string if user not found' do
      expect(RoboPigeon::Slack::Client).to receive(:get_user).with(['test']).and_return(nil)
      expect(subject.slack_user_for('test')).to eq('')
    end
  end

  context 'slack_user_group' do
    it 'calls into the slack client to look up the user and returns a formatted link' do
      expect(subject.slack_user_group('test')).to eq('<!subteam^test>')
    end
  end
end
