describe RoboPigeon::Slack::Message do
  before :each do
    RoboPigeon::Slack::Client.api_key = 'xoxb-token-goes-here'
    RoboPigeon::Slack::Client.name = 'Automated Pigeon Bot'
    RoboPigeon::Slack::Client.emoji = ':pigeon-bot:'
  end
  subject { RoboPigeon::Slack::Message.new }

  it 'throws an error if no message is set' do
    subject.users = ['U0L0MRPB8']
    expect { subject.send! }.to raise_error('No text or attachments set')
  end

  it 'sends the message for each user' do
    VCR.use_cassette('send to users') do
      subject.users = %w[U0L0MRPB8 U0L0MRPB8]
      subject.text = 'Test'
      subject.send!
    end
  end

  it 'sends the message for each channel' do
    VCR.use_cassette('send to channels') do
      subject.channels = ['#testing-slackbots', '#testing-slackbots']
      subject.text = 'Test'
      subject.send!
    end
  end

  it 'uses the name from the client' do
    VCR.use_cassette('send to channels') do
      subject.channels = ['#testing-slackbots']
      subject.text = 'Test'
      expect(RoboPigeon::Slack::Client.client).to receive(:chat_postMessage).with(
        attachments: [],
        channel: '#testing-slackbots',
        icon_emoji: ':pigeon-bot:',
        text: 'Test',
        username: 'Automated Pigeon Bot'
      )
      subject.send!
    end
  end
end
