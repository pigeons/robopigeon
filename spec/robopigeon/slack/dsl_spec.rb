describe RoboPigeon::Dsl::Slack do
  let(:test_message) { RoboPigeon::Slack::Message.new }

  before :each do
    allow(RoboPigeon::Slack::Message).to receive(:new).and_return(test_message)
  end

  it 'sets the text in the dsl' do
    subject.instance_eval do
      text 'Test'
    end
    expect(subject.message.text).to eq('Test')
  end

  it 'adds a channel in the dsl' do
    expect(subject).to receive(:channel).with('#team-pigeon')
    subject.instance_eval do
      channel '#team-pigeon'
    end
  end

  it 'has access to helpers' do
    expect(subject).to receive(:user)
    subject.instance_eval do
      user git_committer_email
    end
  end

  it 'adds a user in the dsl' do
    expect(RoboPigeon::Slack::Client).to receive(:get_user).with(['A Pigeon']).and_return(OpenStruct.new(id: 'U0LM02321'))
    subject.instance_eval do
      user 'A Pigeon'
    end
    expect(subject.message.users).to eq(['U0LM02321'])
  end
end
